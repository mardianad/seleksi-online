<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('admin/Model_siswa');
        $this->load->model('Model_sekolah');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['soal_psikotest'] = $this->Model_soal_psikotest->show();
        $this->load->view('view_header', $data);
        $this->load->view('view_seleksi', $data);
        $this->load->view('view_footer');
    }

    public function pendaftaran()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            $path = $_FILES['foto']['name'];
            $path_tmp = $_FILES['foto']['tmp_name'];
            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $foto = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path_akta = $_FILES['fc_akta_kelahiran']['name'];
            $path_tmp_akta = $_FILES['fc_akta_kelahiran']['tmp_name'];
            if ($path_akta != '') {
                $ext_akta = pathinfo($path_akta, PATHINFO_EXTENSION);
                $akta = basename($path_akta, '.' . $ext_akta);
                $ext_check = $this->Model_common->extension_check_photo($ext_akta);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path_kk = $_FILES['fc_kk']['name'];
            $path_tmp_kk = $_FILES['fc_kk']['tmp_name'];
            if ($path_kk != '') {
                $ext_kk = pathinfo($path_kk, PATHINFO_EXTENSION);
                $kk = basename($path_kk, '.' . $ext_kk);
                $ext_check = $this->Model_common->extension_check_photo($ext_kk);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path_ijazah = $_FILES['fc_ijazah_smk']['name'];
            $path_tmp_ijazah = $_FILES['fc_ijazah_smk']['tmp_name'];
            if ($path_ijazah != '') {
                $ext_ijazah = pathinfo($path_ijazah, PATHINFO_EXTENSION);
                $ijazah = basename($path_ijazah, '.' . $ext_ijazah);
                $ext_check = $this->Model_common->extension_check_photo($ext_ijazah);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            if ($valid == 1) {
                $next_id = $this->Model_siswa->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }
                $foto = "foto-" . $_POST['nama_lengkap'] . '-' . $ai_id . '.' . $ext;
                move_uploaded_file($path_tmp, './public/uploads/siswa/pas_photo/' . $foto);
                $akta = "akta-" . $_POST['nama_lengkap'] . '-' . $ai_id . '.' . $ext_akta;
                move_uploaded_file($path_tmp_akta, './public/uploads/siswa/akta/' . $akta);
                $kk = "KK-" . $_POST['nama_lengkap'] . '-' . $ai_id . '.' . $ext_kk;
                move_uploaded_file($path_tmp_kk, './public/uploads/siswa/kk/' . $kk);
                $ijazah = "ijazah-" . $_POST['nama_lengkap'] . '-' . $ai_id . '.' . $ext_ijazah;
                move_uploaded_file($path_tmp_ijazah, './public/uploads/siswa/ijazah/' . $ijazah);
                $status = "";
                $bmi = $_POST['berat_badan'] / (($_POST['tinggi_badan'] / 100) * ($_POST['tinggi_badan'] / 100));
                if ($bmi < 17 || $bmi > 25) {
                    $status = "Tidak Lulus";
                } else {
                    $status = "Pending";
                }
                $now = date('Y-m-d H:i:s');
                $form_data = array(
                    'nama_lengkap'  => $_POST['nama_lengkap'],
                    'nama_panggilan' => $_POST['nama_panggilan'],
                    'tempat_lahir'  => $_POST['tempat_lahir'],
                    'tanggal_lahir' => $_POST['tanggal_lahir'],
                    'jenis_kelamin' => $_POST['jenis_kelamin'],
                    'no_ktp'        => $_POST['no_ktp'],
                    'asal_sekolah'  => $_POST['asal_sekolah'],
                    'jurusan'       => $_POST['jurusan'],
                    'tahun_lulus'   => $_POST['tahun_lulus'],
                    'agama'         => $_POST['agama'],
                    'suku'          => $_POST['suku'],
                    'nama_jalan'    => $_POST['nama_jalan'],
                    'no_rumah'      => $_POST['no_rumah'],
                    'rt'            => $_POST['rt'],
                    'rw'            => $_POST['rw'],
                    'kelurahan'     => $_POST['kelurahan'],
                    'kecamatan'     => $_POST['kecamatan'],
                    'kota'          => $_POST['kota'],
                    'provinsi'      => $_POST['provinsi'],
                    'mobile_phone'  => $_POST['mobile_phone'],
                    'telephone'     => $_POST['telephone'],
                    'email'         => $_POST['email'],
                    'tanggal_seleksi' => date('Y-m-d'),
                    'tinggi_badan'  => $_POST['tinggi_badan'],
                    'berat_badan'   => $_POST['berat_badan'],
                    'mata_kiri'     => $_POST['mata_kiri'],
                    'mata_kanan'    => $_POST['mata_kanan'],
                    'status_pernikahan' => $_POST['status_pernikahan'],
                    'merokok'       => $_POST['merokok'],
                    'status_identitas_seleksi' => $status,
                    'foto'          => $foto,
                    'fc_akta_kelahiran' => $akta,
                    'fc_kk'         => $kk,
                    'fc_ijazah_smk' => $ijazah,
                    'status'        => 1,
                    'publish'   => 1,
                    'created'  => $now
                );
                $this->Model_siswa->add($form_data);
                if ($status == 'Tidak Lulus') {
                    redirect(base_url() . 'siswa/gagal/' . $ai_id);
                } else {
                    $success = 'Your data is added successfully!';
                    $this->session->set_flashdata('success', $success);
                    redirect(base_url() . 'psikotest/soal/' . $ai_id . '/1');
                }
            } else {
                $success = 'Your data is added successfully!';
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'siswa/pendaftaran');
            }
        } else {
            $data['social'] = $this->Model_common->all_social();

            $data['sekolah'] = $this->Model_sekolah->show();
            $data['jurusan'] = $this->Model_sekolah->showJurusan();
            $this->load->view('view_header', $data);
            $this->load->view('view_pendaftaran', $data);
            $this->load->view('view_footer');
        }
    }
    public function success($siswa_id)
    {
        $data['siswa'] = $this->Model_siswa->getData($siswa_id);
        $data['setting'] = $this->Model_common->all_setting();
        $data['social'] = $this->Model_common->all_social();
        $this->load->view('view_header', $data);
        $this->load->view('view_pendaftaran_success', $data);
        $this->load->view('view_footer');
    }
    public function gagal($siswa_id)
    {
        $data['siswa'] = $this->Model_siswa->getData($siswa_id);
        $data['setting'] = $this->Model_common->all_setting();
        $data['social'] = $this->Model_common->all_social();
        $this->load->view('view_header', $data);
        $this->load->view('view_pendaftaran_gagal', $data);
        $this->load->view('view_footer');
    }
    public function kelulusan()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['social'] = $this->Model_common->all_social();
        $this->load->view('view_header', $data);
        $this->load->view('view_siswa_kelulusan', $data);
        $this->load->view('view_footer');
    }
    public function cek_kelulusan()
    {
        if (isset($_POST['form1'])) {
            $nik = $_POST['nik'];
            $tgl_seleksi = $_POST['tanggal_seleksi'];
            $cek = $this->Model_siswa->getDataKelulusan($nik, $tgl_seleksi);
            if (!empty($cek)) {
                $data['status'] = 1;
            } else {
                $data['status'] = 0;
            }
            $data['siswa'] = $cek;
            $data['setting'] = $this->Model_common->all_setting();
            $data['social'] = $this->Model_common->all_social();
            $this->load->view('view_header', $data);
            $this->load->view('view_siswa_cek_kelulusan', $data);
            $this->load->view('view_footer');
        }
    }
}
