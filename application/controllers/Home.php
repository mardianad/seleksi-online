<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_common');
		$this->load->model('Model_home');
	}

	public function index()
	{
		$data['setting'] = $this->Model_common->all_setting();
		$data['page_home'] = $this->Model_common->all_page_home();
		$data['social'] = $this->Model_common->all_social();

		$data['sliders'] = $this->Model_home->all_slider();


		$this->load->view('view_header', $data);
		$this->load->view('view_home', $data);
		$this->load->view('view_footer', $data);
	}
}
