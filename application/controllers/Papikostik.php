<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Papikostik extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('admin/Model_siswa');
        $this->load->model('admin/Model_soal_papikostik');
        $this->load->model('Model_papikostik');
    }

    public function soal($siswa_id)
    {
        $data['setting'] = $this->Model_common->all_setting();
        $error = '';
        $success = '';
        if (isset($_POST['siswa_id'])) {
            $valid = 1;
            if ($valid == 1) {
                $next_id = $this->Model_papikostik->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }
                $now = date('Y-m-d H:i:s');
                $data = array();
                foreach ($_POST['s'] as $k => $v) {
                    if (!isset($data[$v])) $data[$v] = 0;
                    $data[$v] += 1;
                }
                $values = array();
                foreach ($data as $k => $v) $values[] = "($siswa_id,{$k},{$v},'$now')";
                $this->Model_papikostik->add_result($values);
                $success = 'Your data is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'siswa/success/' . $siswa_id);
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'papikostik/gagal/' . $siswa_id);
            }
        } else {
            $data['social'] = $this->Model_common->all_social();
            $data['soal'] = $this->Model_papikostik->show_soal_array();
            $data['siswa_id'] = $siswa_id;

            $this->load->view('view_header', $data);
            $this->load->view('view_papikostik', $data);
            $this->load->view('view_footer');
        }
    }
    public function success($siswa_id)
    {
        $data['setting'] = $this->Model_common->all_setting();

        $this->load->view('view_header', $data);
        $this->load->view('view_papikostik_success', $data);
        $this->load->view('view_footer');
    }
}
