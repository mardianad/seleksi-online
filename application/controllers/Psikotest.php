<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Psikotest extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('admin/Model_siswa');
        $this->load->model('admin/Model_soal_psikotest');
        $this->load->model('Model_psikotest');
    }

    public function index($siswa_id)
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['bab'] = $this->Model_psikotest->show_bab_limit();

        $this->load->view('view_header', $data);
        $this->load->view('view_psikotest', $data);
        $this->load->view('view_footer');
    }

    public function soal($siswa_id, $bab_id)
    {
        $data['setting'] = $this->Model_common->all_setting();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            if ($valid == 1) {
                $next_id = $this->Model_psikotest->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }
                $now = date('Y-m-d H:i:s');
                $count_jawaban = count($_POST['soal_id']);
                $nilai = 0;
                $jml_soal = count($this->Model_psikotest->show_soal($bab_id));
                $soal = $this->Model_psikotest->show_soal($bab_id);
                $nilai_per_butir = 100 / $jml_soal;
                foreach ($soal as $s) {
                    $jawaban = $_POST['jawaban'][$s['id']];
                    $soal_id = $_POST['soal_id'][$s['id']];
                    echo $soal_id . "=>" . $jawaban . "<br/>";

                    $cek_kj = $this->Model_psikotest->check_kj($soal_id);
                    if ($cek_kj['jawaban'] == $jawaban) {
                        $nilai += $nilai_per_butir;
                    }
                }

                $form_data = array(
                    'bab_id'  => $_POST['bab_id'],
                    'siswa_id' => $_POST['siswa_id'],
                    'nilai'  => $nilai,
                    'created'  => $now
                );
                $this->Model_psikotest->add($form_data);
                $success = 'Your data is added successfully!';
                $this->session->set_flashdata('success', $success);
                $bab_next = $bab_id + 1;
                redirect(base_url() . 'psikotest/soal/' . $siswa_id . '/' . $bab_next);
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'psikotest/index/' . $siswa_id);
            }
        } else {
            $cek_bab = $this->Model_psikotest->check_bab($bab_id);
            $nilai = $this->Model_psikotest->show_nilai($siswa_id);
            if (empty($cek_bab)) {
                $form_siswa = array(
                    'nilai_psikotest' => $nilai['total_nilai']
                );
                $this->Model_siswa->update($siswa_id, $form_siswa);
                if ($nilai['total_nilai'] <= 150) {
                    redirect(base_url() . 'siswa/gagal/' . $siswa_id);
                } else {
                    redirect(base_url() . 'papikostik/soal/' . $siswa_id);
                }
            } else {
                $data['social'] = $this->Model_common->all_social();
                $data['soal'] = $this->Model_psikotest->show_soal($bab_id);
                $data['bab'] = $this->Model_psikotest->check_bab($bab_id);
                $data['siswa_id'] = $siswa_id;
                $data['bab_id'] = $bab_id;
                $this->load->view('view_header', $data);
                $this->load->view('view_psikotest_soal', $data);
                $this->load->view('view_footer');
            }
        }
    }
}
