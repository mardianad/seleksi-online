<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Model_common');
		$this->load->model('admin/Model_dashboard');
	}
	public function index()
	{
		$data['setting'] = $this->Model_common->get_setting_data();
		$from_date_siswa = date('Y-') . "01-01";
		$end_date = date('Y-') . "12-31";
		$siswa_peryear = $this->Model_dashboard->siswa_peryear(date('Y'));
		$sp = array();
		foreach ($siswa_peryear as $s) {
			$sp += array(
				$s['bulan'] => $s['jumlah']
			);
		}
		$data['siswa_peryear'] = (array_replace(
			array_fill(
				date('n', strtotime($from_date_siswa)),
				date('n', strtotime($end_date)),
				0
			),
			$sp
		));
		$siswa_psikotest_peryear = $this->Model_dashboard->siswa_psikotest_peryear(date('Y'));
		$spp = array();
		foreach ($siswa_psikotest_peryear as $s) {
			$spp += array(
				$s['bulan'] => $s['jumlah']
			);
		}
		$data['siswa_psikotest_peryear'] = (array_replace(array_fill(date('n', strtotime($from_date_siswa)), date('n', strtotime($end_date)), 0), $spp));
		$siswa_papi_peryear = $this->Model_dashboard->siswa_papi_peryear(date('Y'));
		$sppp = array();
		foreach ($siswa_papi_peryear as $s) {
			$sppp += array(
				$s['bulan'] => $s['jumlah']
			);
		}
		$data['siswa_papi_peryear'] = (array_replace(array_fill(date('n', strtotime($from_date_siswa)), date('n', strtotime($end_date)), 0), $sppp));
		$siswa_lulus_peryear = $this->Model_dashboard->siswa_lulus_peryear(date('Y'));
		$slp = array();
		foreach ($siswa_lulus_peryear as $s) {
			$slp += array(
				$s['bulan'] => $s['jumlah']
			);
		}
		$data['siswa_lulus_peryear'] = (array_replace(array_fill(date('n', strtotime($from_date_siswa)), date('n', strtotime($end_date)), 0), $slp));
		$this->load->view('admin/view_header', $data);
		$this->load->view('admin/view_dashboard', $data);
		$this->load->view('admin/view_footer');
	}
}
