<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Soal_papikostik extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_soal_papikostik');
    }
    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['papi'] = $this->Model_soal_papikostik->show();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_soal_papikostik', $data);
        $this->load->view('admin/view_footer');
    }
    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            if ($valid == 1) {
                $next_id = $this->Model_soal_papikostik->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }
                $now = date('Y-m-d H:i:s');
                $form_data = array(
                    'question1'         => $_POST['question1'],
                    'value1'       => $_POST['value1'],
                    'question2'         => $_POST['question2'],
                    'value2'       => $_POST['value2']
                );
                $this->Model_soal_papikostik->add($form_data);
                $success = 'Soal papikostik is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/soal_papikostik');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/soal_papikostik/add');
            }
        } else {
            $data['bab'] = $this->Model_soal_papikostik->get_bab();
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_soal_papikostik_add', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function edit($id)
    {
        $tot = $this->Model_soal_papikostik->soal_papi_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/soal_papikostik');
            exit;
        }
        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            if ($valid == 1) {
                $form_data = array(
                    'question1'         => $_POST['question1'],
                    'value1'       => $_POST['value1'],
                    'question2'         => $_POST['question2'],
                    'value2'       => $_POST['value2']
                );
                $this->Model_soal_papikostik->update($id, $form_data);
                $success = 'Soal papikostik is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/soal_papikostik');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/soal_papikostik/add');
            }
        } else {
            $data['papi'] = $this->Model_soal_papikostik->getData($id);
            $data['id'] = $id;
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_soal_papikostik_edit', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function delete($id)
    {
        $tot = $this->Model_soal_papikostik->soal_papi_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/soal_papikostik');
            exit;
        }
        $data['soal_papikostik'] = $this->Model_soal_papikostik->getData($id);
        $this->Model_soal_papikostik->delete($id);
        $success = 'Soal papikostik is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/soal_papikostik');
    }
}
