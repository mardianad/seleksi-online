<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Soal_psikotest extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_soal_psikotest');
    }
    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['soal_psikotest'] = $this->Model_soal_psikotest->show();
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_soal_psikotest', $data);
        $this->load->view('admin/view_footer');
    }

    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];
            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path2 = $_FILES['a_gambar']['name'];
            $path_tmp2 = $_FILES['a_gambar']['tmp_name'];
            if ($path2 != '') {
                $ext = pathinfo($path2, PATHINFO_EXTENSION);
                $file_name = basename($path2, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path3 = $_FILES['b_gambar']['name'];
            $path_tmp3 = $_FILES['b_gambar']['tmp_name'];
            if ($path3 != '') {
                $ext = pathinfo($path3, PATHINFO_EXTENSION);
                $file_name = basename($path3, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path4 = $_FILES['c_gambar']['name'];
            $path_tmp4 = $_FILES['c_gambar']['tmp_name'];
            if ($path4 != '') {
                $ext = pathinfo($path4, PATHINFO_EXTENSION);
                $file_name = basename($path4, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path5 = $_FILES['d_gambar']['name'];
            $path_tmp5 = $_FILES['d_gambar']['tmp_name'];
            if ($path5 != '') {
                $ext = pathinfo($path5, PATHINFO_EXTENSION);
                $file_name = basename($path5, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            $path6 = $_FILES['e_gambar']['name'];
            $path_tmp6 = $_FILES['e_gambar']['tmp_name'];
            if ($path6 != '') {
                $ext = pathinfo($path6, PATHINFO_EXTENSION);
                $file_name = basename($path6, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            if ($valid == 1) {
                $next_id = $this->Model_soal_psikotest->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }
                $final_name = 'soal_psikotest-' . $ai_id . '.' . $ext;
                move_uploaded_file($path_tmp, './public/uploads/psikotest/' . $final_name);
                $final_name_jawaban_1 = 'jawaban_soal_psikotest-' . $ai_id . 'A.' . $ext;
                move_uploaded_file($path_tmp2, './public/uploads/psikotest/' . $final_name_jawaban_1);
                $final_name_jawaban_2 = 'jawaban_soal_psikotest-' . $ai_id . 'B.' . $ext;
                move_uploaded_file($path_tmp3, './public/uploads/psikotest/' . $final_name_jawaban_2);
                $final_name_jawaban_3 = 'jawaban_soal_psikotest-' . $ai_id . 'C.' . $ext;
                move_uploaded_file($path_tmp4, './public/uploads/psikotest/' . $final_name_jawaban_3);
                $final_name_jawaban_4 = 'jawaban_soal_psikotest-' . $ai_id . 'D.' . $ext;
                move_uploaded_file($path_tmp5, './public/uploads/psikotest/' . $final_name_jawaban_4);
                $final_name_jawaban_5 = 'jawaban_soal_psikotest-' . $ai_id . 'E.' . $ext;
                move_uploaded_file($path_tmp6, './public/uploads/psikotest/' . $final_name_jawaban_5);
                $now = date('Y-m-d H:i:s');
                $form_data = array(
                    'bab_id'         => $_POST['bab_id'],
                    'soal'       => $_POST['soal'],
                    'gambar'              => $final_name,
                    'jawaban'   => $_POST['jawaban'],
                    'status'        => 1,
                    'publish'   => 1,
                    'created'  => $now,
                    'created_by'     => $this->session->userdata('id'),
                    'modified'  => $now,
                    'modified_by'     => $this->session->userdata('id')
                );
                $this->Model_soal_psikotest->add($form_data);
                $form_data_jawaban = array(
                    'soal_id'         => $ai_id,
                    'a_jawaban'       => $_POST['a_jawaban'],
                    'a_gambar'              => $final_name_jawaban_1,
                    'b_jawaban'       => $_POST['b_jawaban'],
                    'b_gambar'              => $final_name_jawaban_2,
                    'c_jawaban'       => $_POST['c_jawaban'],
                    'c_gambar'              => $final_name_jawaban_3,
                    'd_jawaban'       => $_POST['d_jawaban'],
                    'd_gambar'              => $final_name_jawaban_4,
                    'e_jawaban'       => $_POST['e_jawaban'],
                    'e_gambar'              => $final_name_jawaban_5,
                    'dipilih'   => $_POST['jawaban'],
                    'status'        => 1,
                    'publish'   => 1,
                    'created'  => $now,
                    'created_by'     => $this->session->userdata('id'),
                    'modified'  => $now,
                    'modified_by'     => $this->session->userdata('id')
                );
                $this->Model_soal_psikotest->add_kj($form_data_jawaban);
                $success = 'Soal Psikotest is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/soal_psikotest');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/soal_psikotest/add');
            }
        } else {
            $data['bab'] = $this->Model_soal_psikotest->get_bab();
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_soal_psikotest_add', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function edit($id)
    {
        $tot = $this->Model_soal_psikotest->soal_psikotest_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/soal_psikotest');
            exit;
        }
        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];
            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }
            $path2 = $_FILES['a_gambar']['name'];
            $path_tmp2 = $_FILES['a_gambar']['tmp_name'];
            if ($path2 != '') {
                $ext = pathinfo($path2, PATHINFO_EXTENSION);
                $file_name = basename($path2, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }
            $path3 = $_FILES['b_gambar']['name'];
            $path_tmp3 = $_FILES['b_gambar']['tmp_name'];
            if ($path3 != '') {
                $ext = pathinfo($path3, PATHINFO_EXTENSION);
                $file_name = basename($path3, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }

            $path4 = $_FILES['c_gambar']['name'];
            $path_tmp4 = $_FILES['c_gambar']['tmp_name'];
            if ($path4 != '') {
                $ext = pathinfo($path4, PATHINFO_EXTENSION);
                $file_name = basename($path4, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }
            $path5 = $_FILES['d_gambar']['name'];
            $path_tmp5 = $_FILES['d_gambar']['tmp_name'];
            if ($path5 != '') {
                $ext = pathinfo($path5, PATHINFO_EXTENSION);
                $file_name = basename($path5, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }
            $path6 = $_FILES['e_gambar']['name'];
            $path_tmp6 = $_FILES['e_gambar']['tmp_name'];
            if ($path6 != '') {
                $ext = pathinfo($path6, PATHINFO_EXTENSION);
                $file_name = basename($path6, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }
            if ($valid == 1) {
                $data['soal_psikotest'] = $this->Model_soal_psikotest->getData($id);
                $now = date('Y-m-d H:i:s');
                if ($path == '') {
                    $form_data = array(
                        'bab_id'         => $_POST['bab_id'],
                        'soal'       => $_POST['soal'],
                        'jawaban'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update($id, $form_data);
                }
                if ($path != '') {
                    unlink('./public/uploads/psikotest/' . $data['soal_psikotest']['gambar']);

                    $final_name = 'soal_psikotest-' . $id . '.' . $ext;
                    move_uploaded_file($path_tmp, './public/uploads/psikotest/' . $final_name);

                    $now = date('Y-m-d H:i:s');
                    $form_data = array(
                        'bab_id'         => $_POST['bab_id'],
                        'soal'       => $_POST['soal'],
                        'jawaban'   => $_POST['jawaban'],
                        'gambar'              => $final_name,
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update($id, $form_data);
                }
                $now = date('Y-m-d H:i:s');
                $jawaban = $this->Model_soal_psikotest->get_kj($id);
                if ($path2 == '') {
                    $form_data_jawaban = array(
                        'a_jawaban'       => $_POST['a_jawaban'],
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                } else {
                    unlink('./public/uploads/psikotest/' . $jawaban['a_gambar']);
                    $final_name_jawaban_1 = 'jawaban_soal_psikotest-' . $id . 'A.' . $ext;
                    move_uploaded_file($path_tmp2, './public/uploads/psikotest/' . $final_name_jawaban_1);
                    $form_data_jawaban = array(
                        'a_jawaban'       => $_POST['a_jawaban'],
                        'a_gambar'              => $final_name_jawaban_1,
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                }
                if ($path3 == '') {
                    $form_data_jawaban = array(
                        'b_jawaban'       => $_POST['b_jawaban'],
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                } else {
                    unlink('./public/uploads/psikotest/' . $jawaban['b_gambar']);
                    $final_name_jawaban_2 = 'jawaban_soal_psikotest-' . $id . 'B.' . $ext;
                    move_uploaded_file($path_tmp3, './public/uploads/psikotest/' . $final_name_jawaban_2);
                    $form_data_jawaban = array(
                        'b_jawaban'       => $_POST['b_jawaban'],
                        'b_gambar'              => $final_name_jawaban_2,
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                }
                if ($path4 == '') {
                    $form_data_jawaban = array(
                        'c_jawaban'       => $_POST['c_jawaban'],
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'created_by'     => $this->session->userdata('id'),
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                } else {
                    unlink('./public/uploads/psikotest/' . $jawaban['c_gambar']);
                    $final_name_jawaban_3 = 'jawaban_soal_psikotest-' . $id . 'C.' . $ext;
                    move_uploaded_file($path_tmp4, './public/uploads/psikotest/' . $final_name_jawaban_3);
                    $form_data_jawaban = array(
                        'c_jawaban'       => $_POST['c_jawaban'],
                        'c_gambar'              => $final_name_jawaban_3,
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                }
                if ($path5 == '') {
                    $form_data_jawaban = array(
                        'd_jawaban'       => $_POST['d_jawaban'],
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                } else {
                    unlink('./public/uploads/psikotest/' . $jawaban['d_gambar']);
                    $final_name_jawaban_4 = 'jawaban_soal_psikotest-' . $id . 'D.' . $ext;
                    move_uploaded_file($path_tmp5, './public/uploads/psikotest/' . $final_name_jawaban_4);
                    $form_data_jawaban = array(
                        'd_jawaban'       => $_POST['d_jawaban'],
                        'd_gambar'              => $final_name_jawaban_4,
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                }
                if ($path6 == '') {
                    $form_data_jawaban = array(
                        'e_jawaban'       => $_POST['e_jawaban'],
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                } else {
                    unlink('./public/uploads/psikotest/' . $jawaban['e_gambar']);
                    $final_name_jawaban_5 = 'jawaban_soal_psikotest-' . $id . 'E.' . $ext;
                    move_uploaded_file($path_tmp6, './public/uploads/psikotest/' . $final_name_jawaban_5);
                    $form_data_jawaban = array(
                        'e_jawaban'       => $_POST['e_jawaban'],
                        'e_gambar'              => $final_name_jawaban_5,
                        'dipilih'   => $_POST['jawaban'],
                        'status'        => 1,
                        'publish'   => 1,
                        'modified'  => $now,
                        'modified_by'     => $this->session->userdata('id')
                    );
                    $this->Model_soal_psikotest->update_kj($id, $form_data_jawaban);
                }
                $success = 'Soal Psikotest is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/soal_psikotest');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/soal_psikotest/add');
            }
        } else {
            $data['soal_psikotest'] = $this->Model_soal_psikotest->getData($id);
            $data['jawaban'] = $this->Model_soal_psikotest->get_kj($id);
            $data['bab'] = $this->Model_soal_psikotest->get_bab();
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_soal_psikotest_edit', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function delete($id)
    {
        $tot = $this->Model_soal_psikotest->soal_psikotest_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/soal_psikotest');
            exit;
        }
        $data['soal_psikotest'] = $this->Model_soal_psikotest->getData($id);
        if ($data['soal_psikotest']) {
            unlink('./public/uploads/psikotest/' . $data['soal_psikotest']['gambar']);
        }
        $this->Model_soal_psikotest->delete($id);
        $success = 'Soal Psikotest is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/soal_psikotest');
    }
}
