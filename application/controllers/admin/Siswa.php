<?php
defined('BASEPATH') or exit('No direct script access allowed');

require('./vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Siswa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_siswa');
        $this->load->model('admin/Model_interview');
        $this->load->model('admin/Model_soal_psikotest');
        $this->load->model('admin/Model_soal_papikostik');
        $this->load->helper('tgl_indo');
    }
    public function index($status = null)
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        if ($status == null) {
            $status = 1;
        } else {
            $status = $status;
        }
        if ($status == 1) {
            $data['status'] = "Lulus";
            $data['siswa'] = $this->Model_siswa->show_siswa_by_status('Lulus Seleksi');
        } else {
            $data['status'] = "Tidak Lulus";
            $data['siswa'] = $this->Model_siswa->show_siswa_by_status('Tidak Lulus');
        }
        $data['status_id'] = $status;
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa', $data);
        $this->load->view('admin/view_footer');
    }
    public function pendaftar()
    {
        $this->load->library('pagination');

        $config['base_url'] = site_url('/admin/siswa/pendaftar');
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Model_siswa->showCount();
        $config['per_page'] = 5; // <-kamu bisa ubah ini
        $config['next_link'] = 'Selanjutnya';
        $config['prev_link'] = 'Sebelumnya';
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $limit = $config['per_page'];
        $offset = html_escape($this->input->get('per_page'));

        $data['setting'] = $this->Model_common->get_setting_data();
        $data['siswa'] = $this->Model_siswa->show($limit, $offset);
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa_all', $data);
        $this->load->view('admin/view_footer');
    }
    public function report($status, $report = null, $fconditions = null)
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $status = $status;
        if (isset($_POST['filter'])) {
            if ($status == 1) {
                $data['status'] = "Lulus Seleksi";
            } else {
                $data['status'] = "Tidak Lulus";
            }
            $_POST = array_filter($_POST);
            foreach ($_POST as $k => $v) {
                if ($k == "tanggal_seleksi") {
                    $range_date = implode("' and '", $v);
                    $fconditions .= " AND " . $k . " between '" . $range_date . "'";
                }
            }
            $data['status_id'] = $status;
            $data['fconditions'] = $fconditions;
            $data['siswa'] = $this->Model_siswa->showByFilter($data['status'], $fconditions);
        } else {
            if ($fconditions == null) {
                $data['fconditions'] = null;
                if ($status == 1) {
                    $data['status'] = "Lulus Seleksi";
                    $data['siswa'] = $this->Model_siswa->show_siswa_by_status('Lulus Seleksi');
                } else {
                    $data['status'] = "Tidak Lulus";
                    $data['siswa'] = $this->Model_siswa->show_siswa_by_status('Tidak Lulus');
                }
                $data['status_id'] = $status;
            } else {
                $data['fconditions'] = $fconditions;
                $conditionss = str_replace("%20", " ", $fconditions);
                $conditions = str_replace("%27", "'", $conditionss);
                if ($status == 1) {
                    $data['status'] = "Lulus Seleksi";
                } else {
                    $data['status'] = "Tidak Lulus";
                }
                $data['status_id'] = $status;
                $data['siswa'] = $this->Model_siswa->showByFilter($data['status'], $conditions);
            }
        }
        $data['idstatus'] = $status;
        if ($report != null) {
            $objPHPExcel  = new Spreadsheet();
            $objPHPExcel->getProperties()->setCreator('Mardiana')
                ->setLastModifiedBy('Mardiana')
                ->setTitle('Office 2007 XLSX Test Document')
                ->setSubject('Office 2007 XLSX Test Document')
                ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
                ->setKeywords('office 2007 openxml php')
                ->setCategory('Test result file');
            $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $logo = './public/uploads/favicon-logo.png';
            $objDrawing->setPath($logo);
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(50);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B1', "PT JAPAN INDONESIAN ECONOMIC CENTER");
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setItalic(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->mergeCells('B1:F2');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', "Daftar Peserta " . $data['status']);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setHorizontalCentered(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->mergeCells('A4:F4');
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ];
            $styleArray2 = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ];
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(27, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(28, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(28, 'mm');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A5', "No")
                ->setCellValue('B5', "Nama Lengkap")
                ->setCellValue('C5', "Nama Panggilan")
                ->setCellValue('D5', "Sekolah")
                ->setCellValue('E5', "Jurusan")
                ->setCellValue('F5', "Tanggal Seleksi");
            $objPHPExcel->getActiveSheet()->getStyle('A5:F5')->applyFromArray($styleArray);
            $row = 6;
            $no = 1;
            foreach ($data['siswa'] as $dt) {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $no)
                    ->setCellValue('B' . $row, $dt['nama_lengkap'])
                    ->setCellValue('C' . $row, $dt['nama_panggilan'])
                    ->setCellValue('D' . $row, $dt['asal_sekolah'])
                    ->setCellValue('E' . $row, $dt['jurusan'])
                    ->setCellValue('F' . $row, $dt['tanggal_seleksi']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':F' . $row)->applyFromArray($styleArray2);
                $row++;
                $no++;
            }
            $n = $row + 1;
            $nn = $n + 4;
            if ($status == 1) {
                $namefile = "ReportPesertaLulusSeleksi.xlsx";
            } else {
                $namefile = "ReportPesertaTidakLulusSeleksi.xlsx";
            }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('F' . $n, "Depok, " . date_indo(date("Y-m-d")))
                ->setCellValue('F' . $nn, "Tenten Mangku Sapuan");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $namefile . '"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
            $writer->save('php://output');
            exit;
        } else {
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_siswa_laporan', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function siswa_psikotest($type = null)
    {
        if ($type == null) {
            $type = 1;
        } else {
            $type = $type;
        }
        if ($type == 1) {
            $data['siswa'] = $this->Model_siswa->show_siswa_lulus_psikotest();
        } else {
            $data['siswa'] = $this->Model_siswa->show_siswa_tidak_lulus_psikotest();
        }
        $data['setting'] = $this->Model_common->get_setting_data();
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa_psikotest', $data);
        $this->load->view('admin/view_footer');
    }
    public function report_psikotest($report = null, $fconditions = null)
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $status = 1;
        if (isset($_POST['filter'])) {
            if ($status == 1) {
                $data['status'] = "Lulus Seleksi";
            } else {
                $data['status'] = "Tidak Lulus";
            }
            $_POST = array_filter($_POST);
            foreach ($_POST as $k => $v) {
                if ($k == "tanggal_seleksi") {
                    $range_date = implode("' and '", $v);
                    $fconditions .= " AND " . $k . " between '" . $range_date . "'";
                }
            }
            $data['fconditions'] = $fconditions;
            if ($status == 1) {
                $data['siswa'] = $this->Model_siswa->showByFilter_psikotest_lulus($fconditions);
            } else {
                $data['siswa'] = $this->Model_siswa->showByFilter_psikotest_tidak_lulus($fconditions);
            }
        } else {
            if ($fconditions == null) {
                $data['fconditions'] = null;
                if ($status == 1) {
                    $data['siswa'] = $this->Model_siswa->show_siswa_lulus_psikotest();
                } else {
                    $data['siswa'] = $this->Model_siswa->show_siswa_tidak_lulus_psikotest();
                }
            } else {
                $data['fconditions'] = $fconditions;
                $conditionss = str_replace("%20", " ", $fconditions);
                $conditions = str_replace("%27", "'", $conditionss);
                if ($status == 1) {
                    $data['siswa'] = $this->Model_siswa->showByFilter_psikotest_lulus($conditions);
                } else {
                    $data['siswa'] = $this->Model_siswa->showByFilter_psikotest_tidak_lulus($conditions);
                }
            }
        }
        $data['idstatus'] = $status;
        if ($report != null) {
            $objPHPExcel  = new Spreadsheet();
            $objPHPExcel->getProperties()->setCreator('Mardiana')
                ->setLastModifiedBy('Mardiana')
                ->setTitle('Office 2007 XLSX Test Document')
                ->setSubject('Office 2007 XLSX Test Document')
                ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
                ->setKeywords('office 2007 openxml php')
                ->setCategory('Test result file');
            $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $logo = './public/uploads/favicon-logo.png';
            $objDrawing->setPath($logo);
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(50);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B1', "PT JAPAN INDONESIAN ECONOMIC CENTER");
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setItalic(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->mergeCells('B1:G2');
            $objPHPExcel->getActiveSheet()->mergeCells('A1:A2');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', "Daftar Nilai Psikotest Siswa");
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setHorizontalCentered(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getFont()->setSize(12);
            $objPHPExcel->getActiveSheet()->mergeCells('A4:G4');
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ];
            $styleArray2 = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ];
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(27, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(28, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(28, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(28, 'mm');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A5', "No")
                ->setCellValue('B5', "Nama Lengkap")
                ->setCellValue('C5', "Nama Panggilan")
                ->setCellValue('D5', "Sekolah")
                ->setCellValue('E5', "Jurusan")
                ->setCellValue('F5', "Tanggal Seleksi")
                ->setCellValue('G5', "Nilai Psikotest");
            $objPHPExcel->getActiveSheet()->getStyle('A5:G5')->applyFromArray($styleArray);
            $row = 6;
            $no = 1;
            foreach ($data['siswa'] as $dt) {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $no)
                    ->setCellValue('B' . $row, $dt['nama_lengkap'])
                    ->setCellValue('C' . $row, $dt['nama_panggilan'])
                    ->setCellValue('D' . $row, $dt['asal_sekolah'])
                    ->setCellValue('E' . $row, $dt['jurusan'])
                    ->setCellValue('F' . $row, $dt['tanggal_seleksi'])
                    ->setCellValue('G' . $row, $dt['total_nilai']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getAlignment()->setWrapText(true);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':G' . $row)->applyFromArray($styleArray2);
                $row++;
                $no++;
            }
            $n = $row + 1;
            $nn = $n + 4;
            $now = date('Y-m-d');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('G' . $n, "Depok, " . date_indo($now))
                ->setCellValue('G' . $nn, "Tenten Mangku Sapuan");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="ReportNilaiPsikotest.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
            $writer->save('php://output');
            exit;
        } else {
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_siswa_psikotest_laporan', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function siswa_papikostik()
    {
        $data['siswa'] = $this->Model_siswa->show_papikostik();
        $data['setting'] = $this->Model_common->get_setting_data();
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa_papikostik', $data);
        $this->load->view('admin/view_footer');
    }
    public function siswa_result_papikostik($siswa_id, $report = null)
    {
        $data['papi_result'] = $this->Model_siswa->show_result_papi($siswa_id);
        $data['siswa'] = $this->Model_siswa->getData($siswa_id);
        $data['siswa_id'] = $siswa_id;
        $data['setting'] = $this->Model_common->get_setting_data();
        if ($report != null) {
            $objPHPExcel  = new Spreadsheet();
            $objPHPExcel->getProperties()->setCreator('Mardiana')
                ->setLastModifiedBy('Mardiana')
                ->setTitle('Office 2007 XLSX Test Document')
                ->setSubject('Office 2007 XLSX Test Document')
                ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
                ->setKeywords('office 2007 openxml php')
                ->setCategory('Test result file');
            $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $logo = './public/uploads/favicon-logo.png';
            $objDrawing->setPath($logo);
            $objDrawing->setCoordinates('C1');
            $objDrawing->setHeight(50);
            $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('D1', "PT JAPAN INDONESIAN ECONOMIC CENTER");
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setItalic(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $siswa = $data['siswa'];
            $objDrawing2 = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
            $objDrawing2->setName('Logo');
            $objDrawing2->setDescription('Logo');
            $logo2 = './public/uploads/siswa/pas_photo/' . $siswa['foto'];
            $objDrawing2->setPath($logo2);
            $objDrawing2->setCoordinates('A4');
            $objDrawing2->setHeight(200);
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $objDrawing2->setWorksheet($objPHPExcel->getActiveSheet());
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A14', "Interprestation Result");
            $objPHPExcel->getActiveSheet()->getStyle('A14')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setHorizontalCentered(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('A14')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A14')->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->mergeCells('A14:D14');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('C5', $siswa['nama_lengkap']);
            $objPHPExcel->getActiveSheet()->getStyle('C5:C7')->getFont()->setBold(TRUE);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setHorizontalCentered(TRUE);
            $objPHPExcel->getActiveSheet()->getStyle('C5:C7')->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->mergeCells('C5:D5');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('C6', $siswa['asal_sekolah'] . " (" . $siswa['jurusan'] . ")")
                ->setCellValue('C7', $siswa['tempat_lahir'] . " (" . $siswa['tanggal_lahir'] . ")");
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ];
            $styleArray2 = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ]
            ];
            $styleArray3 = [
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ],
                'borders' => [

                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],

                ]
            ];
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(17, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(27, 'mm');
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50, 'mm');
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A15', "No")
                ->setCellValue('B15', "Aspect")
                ->setCellValue('C15', "Role")
                ->setCellValue('D15', "Result");
            $objPHPExcel->getActiveSheet()->getStyle('A15:D15')->applyFromArray($styleArray);
            $row = 15;
            $no = 1;
            $aspects = $this->Model_soal_papikostik->get_bab();
            foreach ($aspects as $r) {
                $papi_result = $this->Model_siswa->show_result_papi_by_aspect($siswa_id, $r['id']);
                $c_pr = count($papi_result) * 2 + 1;
                $n = $row + 1;
                $np = $n + 1;
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row, $no++)
                    ->setCellValue('B' . $row, $r['aspect']);
                $objPHPExcel->getActiveSheet()->getStyle('A' . $row . ':D' . $row)->applyFromArray($styleArray2);
                foreach ($papi_result as $dt) {
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('C' . $n, $dt['role'] . $r['id'])
                        ->setCellValue('D' . $np, $dt['interprestation']);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('D' . $np)->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A' . $n . ':D' . $np)->applyFromArray($styleArray3);
                    $n += 2;
                    $np += 2;
                }
                $row += $c_pr;
            }
            $n = $row + 1;
            $nn = $n + 4;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('D' . $n, "Depok, " . date_indo(date("Y-m-d")))
                ->setCellValue('D' . $nn, "Tenten Mangku Sapuan");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="Interprestation Result ' . $data['siswa']['nama_lengkap'] . '.xlsx"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $writer = IOFactory::createWriter($objPHPExcel, 'Xlsx');
            $writer->save('php://output');
            exit;
        }
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa_papikostik_result', $data);
        $this->load->view('admin/view_footer');
    }
    public function detail_siswa($siswa_id)
    {
        $bab = count($this->Model_soal_psikotest->get_bab());
        $data['psikotest'] = $this->Model_siswa->get_nilai_psikotest($siswa_id);
        $data['papi_result'] = $this->Model_siswa->show_result_papi($siswa_id);
        $data['siswa'] = $this->Model_siswa->getData($siswa_id);
        $data['setting'] = $this->Model_common->get_setting_data();
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa_detail', $data);
        $this->load->view('admin/view_footer');
    }
    public function siswa_lulus()
    {
        $data['siswa'] = $this->Model_siswa->show_siswa_lulus();
        $data['setting'] = $this->Model_common->get_setting_data();
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_siswa_lulus', $data);
        $this->load->view('admin/view_footer');
    }
    public function wawancara($siswa_id)
    {
        $tot = $this->Model_siswa->siswa_check($siswa_id);
        if (!$tot) {
            redirect(base_url() . 'admin/dashboard');
            exit;
        }
        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            $now = date('Y-m-d H:i:s');
            if ($valid == 1) {
                $nilai_wawancara = $_POST['nilai_sikap'] + $_POST['nilai_sifat'] + $_POST['nilai_komunikasi'];
                if ($nilai_wawancara < 10) {
                    $status = 'Tidak Lulus';
                } else {
                    $status = 'Lulus Seleksi';
                }
                $form_data = array(
                    'interviewer'         => $this->session->userdata('nama'),
                    'tanggal_wawancara'       => $_POST['tanggal_wawancara'],
                    'status_identitas_seleksi'         => $status,
                    'nilai_interview'   => $nilai_wawancara,
                    'catatan'       => $_POST['catatan'],
                    'modified'  => $now,
                    'modified_by'     => $this->session->userdata('id')
                );
                $this->Model_siswa->update($siswa_id, $form_data);
                $form_nilai = array(
                    'siswa_id'         => $siswa_id,
                    'nilai_sikap'       => $_POST['nilai_sikap'],
                    'nilai_sifat'         => $_POST['nilai_sifat'],
                    'nilai_komunikasi'   => $_POST['nilai_komunikasi'],
                    'modified'  => $now,
                    'modified_by'     => $this->session->userdata('id')
                );
                $nilai = $this->Model_interview->showByID($siswa_id);;
                if (empty($nilai)) {
                    $this->Model_interview->add($form_nilai);
                } else {
                    $this->Model_interview->update($siswa_id, $form_nilai);
                }
                $success = 'Data Siswa is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/siswa/siswa_papikostik/' . $siswa_id);
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/siswa/wawancara/' . $siswa_id);
            }
        } else {
            $data['siswa'] = $this->Model_siswa->getData($siswa_id);
            $data['nilai'] = $this->Model_interview->showByID($siswa_id);
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_siswa_add_wawancara', $data);
            $this->load->view('admin/view_footer');
        }
    }
}
