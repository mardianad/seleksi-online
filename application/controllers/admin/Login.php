<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_login');
    }

    public function index()
    {
        $error = '';
        $data['setting'] = $this->Model_login->get_setting_data();
        if (isset($_POST['form1'])) {
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $un = $this->Model_login->check_email($email);
            if (!$un) {
                $error = 'Email address is wrong!';
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin');
            } else {
                $pw = $this->Model_login->check_password($email, $password);
                if (!$pw) {
                    $error = 'Password is wrong!';
                    $this->session->set_flashdata('error', $error);
                    redirect(base_url() . 'admin');
                } else {
                    $array = array(
                        'id' => $pw['id'],
                        'email' => $pw['email'],
                        'nama' => $pw['nama'],
                        'password' => $pw['password'],
                        'photo' => $pw['photo'],
                        'role' => $pw['role'],
                        'status' => $pw['status']
                    );
                    $this->session->set_userdata($array);
                    redirect(base_url() . 'admin/dashboard');
                }
            }
        } else {
            $this->load->view('admin/view_login_2', $data);
        }
    }
    function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url() . 'admin');
    }
}
