<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_profile');
    }
    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['user'] = $this->Model_profile->show();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_user', $data);
        $this->load->view('admin/view_footer');
    }
    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];
            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }
            if ($valid == 1) {
                $next_id = $this->Model_profile->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }
                $final_name = 'user-' . $ai_id . '.' . $ext;
                move_uploaded_file($path_tmp, './public/uploads/' . $final_name);
                $form_data = array(
                    'email' => $_POST['email'],
                    'password' => md5($_POST['password']),
                    'nama'  => $_POST['nama'],
                    'photo' => $final_name,
                    'role'  => 'Admin',
                    'status' => 'Active'
                );
                $this->Model_profile->add($form_data);
                $success = 'User is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/user');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/user/add');
            }
        } else {
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_user_add', $data);
            $this->load->view('admin/view_footer');
        }
    }
    public function update($id)
    {
        $tot = $this->Model_profile->user_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/user');
            exit;
        }
        $error = '';
        $success = '';
        if (isset($_POST['form1'])) {
            $valid = 1;
            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];
            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $data['error'] = 'You must have to upload jpg, jpeg, gif or png file<br>';
                }
            } else {
                $valid = 0;
                $data['error'] = 'You must have to select a photo<br>';
            }
            if ($valid == 1) {
                if ($path == '') {
                    $form_data = array(
                        'email' => $_POST['email'],
                        'nama'  => $_POST['nama']
                    );
                    $this->Model_profile->update_user($id, $form_data);
                }
                if ($path != '') {
                    $user = $this->Model_profile->getData($id);
                    unlink('./public/uploads/' . $user['photo']);
                    $final_name = 'user-' . $id . '.' . $ext;
                    move_uploaded_file($path_tmp, './public/uploads/' . $final_name);
                    $form_data = array(
                        'email' => $_POST['email'],
                        'nama'  => $_POST['nama'],
                        'photo' => $final_name
                    );
                    $this->Model_profile->update_user($id, $form_data);
                }
                $success = 'User is updated successfully!';
                $this->session->set_userdata($form_data);
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/user');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/user');
            }
        }
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['user'] = $this->Model_profile->getData($id);
        $data['id'] = $id;
        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_user_edit', $data);
        $this->load->view('admin/view_footer');
    }
    public function delete($id)
    {
        $tot = $this->Model_profile->user_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/user');
            exit;
        }
        $data['user'] = $this->Model_profile->getData($id);
        if ($data['user']) {
            unlink('./public/uploads/' . $data['user']['photo']);
        }
        $this->Model_profile->delete($id);
        $success = 'User is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/user');
    }
}
