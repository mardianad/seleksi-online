<style type="text/css">
    label>input {
        visibility: hidden;
        position: absolute;
    }

    label>input+img {
        cursor: pointer;
        border: 2px solid transparent;
    }

    label>input:checked+img {
        border: 2px solid #f00;
    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<div class="service-page pt_60 pb_90">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Psikotest</h1>
                <br />
            </div>
        </div>
        <?php echo form_open_multipart(base_url() . 'psikotest/soal/' . $siswa_id . '/' . $bab_id); ?>
        <?php
        $no = 1;
        foreach ($soal as $s) {
            $no1 = 1;
            $no2 = 2;
            $no3 = 3;
            $no4 = 4;
            $no5 = 5; ?>
            <div class="panel panel-default">
                <div class="panel-heading"><?= $no; ?></div>
                <div class="panel-body">
                    <div class="form-row row">
                        <div class="col-lg-12">
                            <div class="services-item effect-item">
                                <input type="hidden" name="soal_id[<?php echo $s['id'] ?>]" value="<?= $s['id'] ?>"><?= $s['id'] ?>
                                <?php if ($s['gambar'] != NULL && $s['gambar'] != "") { ?>
                                    <center>
                                        <a class="image-effect">
                                            <img src="<?php echo base_url(); ?>public/uploads/psikotest/<?php echo $s['gambar']; ?>" width="300px" style="min-height:100px">
                                        </a>
                                    </center>
                                <?php } ?>
                                <?php if ($s['soal'] != NULL && $s['soal'] != "") { ?>
                                    <div class="services-text">
                                        <h3><?= $s['soal']; ?></h3>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        $pilihan = $this->Model_psikotest->show_pilihan_soal($s['id']);
                        ?>
                        <div class="col-lg-2">
                            <div class="services-item effect-item">
                                <label>
                                    <input type="radio" name="jawaban[<?php echo $s['id'] ?>]" value="A" id="radio<?php echo $s['id'] . $no1; ?>">
                                    <?php if ($pilihan['a_gambar'] != NULL && $pilihan['a_gambar'] != "") { ?>

                                        <img src="<?php echo base_url(); ?>public/uploads/psikotest/<?php echo $pilihan['a_gambar']; ?>" width="100px" id="radio<?php echo $s['id'] . $no1; ?>">

                                    <?php } ?>
                                    <?php if ($pilihan['a_jawaban'] != NULL && $pilihan['a_jawaban'] != "") { ?>
                                        <div class="services-text">
                                            <h3><?= $pilihan['a_jawaban']; ?></h3>
                                        </div>
                                    <?php } ?>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="services-item effect-item">
                                <label>
                                    <input type="radio" name="jawaban[<?php echo $s['id'] ?>]" value="B" id="radio<?php echo $s['id'] . $no2; ?>">

                                    <?php if ($pilihan['b_gambar'] != NULL && $pilihan['b_gambar'] != "") { ?>
                                        <img src="<?php echo base_url(); ?>public/uploads/psikotest/<?php echo $pilihan['b_gambar']; ?>" style="width:100px;" id="radio<?php echo $s['id'] . $no2; ?>">
                                    <?php } ?>
                                    <?php if ($pilihan['b_jawaban'] != NULL && $pilihan['b_jawaban'] != "") { ?>
                                        <div class="services-text">
                                            <h3><?= $pilihan['b_jawaban']; ?></h3>
                                        </div>
                                    <?php } ?>
                            </div>
                            </label>
                        </div>
                        <div class="col-lg-3">
                            <div class="services-item effect-item">
                                <label>
                                    <input type="radio" name="jawaban[<?php echo $s['id'] ?>]" value="C" id="radio<?php echo $s['id'] . $no3; ?>">
                                    <?php if ($pilihan['c_gambar'] != NULL && $pilihan['c_gambar'] != "") { ?>
                                        <img src="<?php echo base_url(); ?>public/uploads/psikotest/<?php echo $pilihan['c_gambar']; ?>" style="width:100px;" id="radio<?php echo $s['id'] . $no3; ?>">

                                    <?php } ?>
                                    <?php if ($pilihan['c_jawaban'] != NULL && $pilihan['c_jawaban'] != "") { ?>
                                        <div class="services-text">
                                            <h3><?= $pilihan['c_jawaban']; ?></h3>
                                        </div>
                                    <?php } ?>
                            </div>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <div class="services-item effect-item">
                                <label>
                                    <input type="radio" name="jawaban[<?php echo $s['id'] ?>]" value="D" id="radio<?php echo $s['id'] . $no4; ?>">
                                    <?php if ($pilihan['d_gambar'] != NULL && $pilihan['d_gambar'] != "") { ?>
                                        <img src="<?php echo base_url(); ?>public/uploads/psikotest/<?php echo $pilihan['d_gambar']; ?>" style="width:100px;" id="radio<?php echo $s['id'] . $no4; ?>">

                                    <?php } ?>
                                    <?php if ($pilihan['d_jawaban'] != NULL && $pilihan['d_jawaban'] != "") { ?>
                                        <div class="services-text">
                                            <h3><?= $pilihan['d_jawaban']; ?></h3>
                                        </div>
                                    <?php } ?>
                            </div>
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <div class="services-item effect-item">
                                <label>
                                    <input type="radio" name="jawaban[<?php echo $s['id'] ?>]" value="E" id="radio<?php echo $s['id'] . $no5; ?>">
                                    <?php if ($pilihan['e_gambar'] != NULL && $pilihan['e_gambar'] != "") { ?>
                                        <img src="<?php echo base_url(); ?>public/uploads/psikotest/<?php echo $pilihan['e_gambar']; ?>" style="width:100px;" id="radio<?php echo $s['id'] . $no5; ?>">

                                    <?php } ?>
                                    <?php if ($pilihan['e_jawaban'] != NULL && $pilihan['e_jawaban'] != "") { ?>
                                        <div class="services-text">
                                            <h3><?= $pilihan['e_jawaban']; ?></h3>
                                        </div>
                                    <?php } ?>
                            </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            $no++;
        } ?>
        <input type="hidden" name="siswa_id" value=<?= $siswa_id; ?>>
        <input type="hidden" name="bab_id" value=<?= $bab_id; ?>>
        <div class="form-group col-12">
            <button type="submit" class="btn btn-fill btn-primary" name="form1" onclick="warning_before_save()">Submit</button>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>