<div class="content-wrapper text-center">
    <div class="content">
        <div class="container my-auto">
            <div class="row pt-5">
                <div class="col-12 mx-auto">
                    <img src="" />
                    <img src="<?= base_url('public/uploads/jiaec-img.png'); ?>" width="250" height="250" />
                    <br /> <br />
                    <h1 class="display-4 pt-3">INFORMASI KELULUSAN </h1>
                    <br />
                    <h3>Tes Seleksi Pemagang ke Jepang</h3>
                    <br />
                    <hr />
                </div>
            </div>
            <br /> <br />
            <div class="row pt-2">
                <div class="col-12 col-lg-8 mx-auto">
                    <h2><b>CEK HASIL</b></h2>
                    <br />
                    <?php echo form_open_multipart(base_url() . 'siswa/cek_kelulusan/'); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="form-row row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <h3><label>Cari Berdasarkan NIK dan Tanggal Seleksi</label></h3>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>NIK :</label>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="nik" required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Tanggal Seleksi :</label>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="datepicker" name="tanggal_seleksi" required>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-fill btn-primary" name="form1">Cari</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>