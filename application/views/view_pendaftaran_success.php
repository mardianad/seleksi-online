<!--About Start-->
<div class="about-page pt_60 pb_30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Halo <b><?= $siswa['nama_lengkap']; ?> </b>!</h1>
                <br />
                <h4>
                    Datamu sedang diproses!
                    <br />
                    Silahkan menunggu minimal 7 hari dari sekarang untuk memeriksa status kelulusan anda pada menu kelulusan.
                </h4>
                <br />
            </div>
        </div>
    </div>
</div>
<!--About End-->