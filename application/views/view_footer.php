<div style="background-color: black)">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-12">
                <div class="call-text">
                    <h3><?php echo $setting['cta_text']; ?></h3>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="footer-area pt_60 pb_90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="footer-item">
                    <h3><?php echo $setting['footer_col4_title']; ?></h3>
                    <div class="footer-address-item">
                        <div class="icon"><i class="fa fa-map-marker"></i></div>
                        <div class="text">
                            <span>
                                <?php echo nl2br($setting['footer_address']); ?>
                            </span>
                        </div>
                    </div>
                    <div class="footer-address-item">
                        <div class="icon"><i class="fa fa-phone"></i></div>
                        <div class="text">
                            <span>
                                <?php echo nl2br($setting['footer_phone']); ?>
                            </span>
                        </div>
                    </div>
                    <div class="footer-address-item">
                        <div class="icon"><i class="fa fa-envelope-o"></i></div>
                        <div class="text">
                            <span>
                                <?php echo nl2br($setting['footer_email']); ?>
                            </span>
                        </div>
                    </div>
                    <ul class="footer-social">
                        <?php
                        foreach ($social as $row) {
                            if ($row['social_url'] != '') {
                                echo '<li><a href="' . $row['social_url'] . '"><i class="' . $row['social_icon'] . '"></i></a></li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-bottom pt_50 pb_50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="copy-text">
                    <p>
                        <?php echo $setting['footer_copyright']; ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="scroll-top">
    <i class="fa fa-angle-up"></i>
</div>
<script src="<?php echo base_url(); ?>public/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/select2.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.meanmenu.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.filterizr.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/waypoints.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/viewportchecker.js"></script>
<script src="<?php echo base_url(); ?>public/js/custom.js"></script>
<script>
    function warning_before_save() {
        confirm("Apakah anda yakin data yang diinput sudah benar?");
    }
    $(document).ready(function() {
        $('.select2').select2();
    });
    var currentTab = 0;
    showTab(currentTab);
</script>

</body>

</html>