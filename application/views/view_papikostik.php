<style type="text/css">
    #first {
        padding-left: 16px !important;
    }

    .notb-margin {
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        display: none
    }

    input[type="radio"] {
        visibility: visible;
    }
</style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/w3.css">
<div class="service-page pt_60 pb_90">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">Petunjuk Pengisian</div>
            <div class="panel-body pl_60">
                <p>
                    <ul>
                        <li>Tes ini terdiri dari 90 pasang pernyataan yang berhubungan dengan situasi kerja Saudara.</li>
                        <li>Dari sepasang pernyataan tersebut, Saudara diminta untuk memilih salah satu pernyataan yang paling menggambarkan diri Saudara atau pernyataan mana yang dirasa paling penting bagi Saudara</li>
                        <li>Jika kedua pernyataan tersebut sangat sesuai dengan diri Saudara, maka Saudara tetap harus memilih salah satu diantaranya yang dirasa paling sesuai dengan diri Saudara</li>
                        <li>Hal sebaliknya pun berlaku. Jika kedua pernyataan tersebut sangat tidak sesuai dengan diri Saudara, maka Saudara tetap harus memilih salah satu pernyataan yang paling menggambarkan kondisi diri Saudara yang sebenarnya.</li>
                        <li>Sudara harus menjawabnya dengan jujur dan jangan pernah berpikir untuk memberikan jawaban yang benar, karena jawaban terbaik adalah jawaban yang paling mendekati diri Saudara.</li>
                        <li>Setiap nomor hanya terdiri dari satu jawaban dan tes ini membutuhkan jawaban yang segera (tanpa mempertimbangkan pernyataan yang ada terlalu lama), jadi kerjakanlah secepat-cepatnya namun tetap teliti. Jangan ada yang double atau kosong pada setiap nomor</li>
                    </ul>
                </p>
            </div>
        </div>
        <?php
        $view_num = 5;
        $total_page = ceil(90 / $view_num);
        if (!isset($_SESSION['papiq'])) {
            $result = $soal;
            $no = 0;
            $data = array();
            foreach ($result as $r) {
                $data[++$no] = array($r['value1'], $r['question1'], $r['value2'], $r['question2']);
            }
            $_SESSION['papiq'] = $data;
            unset($data);
        }
        ?>
        <div class="w3-card-4">
            <header class="w3-container w3-blue">
                <h1>PAPI KOSTICK</h1>
            </header>
            <div class="w3-panel w3-red w3-display-container notb-margin" id="alert"><span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">&times;</span>
                <h3>Warning!</h3>
                <p id='msg'></p>
            </div>
            <div id="dynamic_content">
                <div class="w-container" id="page3">
                    <?php echo form_open_multipart(base_url() . 'papikostik/soal/' . $siswa_id, array('onsubmit' => 'return check()')); ?>
                    <div class="w3-panel w3-red w3-display-container notb-margin" id="alert">
                        <span onclick="this.parentElement.style.display='none'" class="w3-button w3-red w3-large w3-display-topright">&times;</span>
                        <h3>Warning!</h3>>
                        <p id='msg'></p>
                    </div>
                    <?php $x = 0;
                    foreach ($_SESSION['papiq'] as $no => $data) {
                        if (($no - 1) % $view_num == 0 || $no == 1) {
                            echo ($no > 1 ? "</table>" : "") . "<table  class='w3-table-all' id='t" . ($x++) . "'  style='width:100%;" . ($no != 1 ? "display:none;'" : "'") . "><tr class='w3-theme-d1'><th>No</th><th colspan='2'>Pernyataan</th></tr>";
                        }
                        echo "<tr>
                                <td rowspan='2'>[{$no}]</td>
                                <td class='pl_15'><input type='radio' id='s_{$no}_0' name='s[{$no}]' value='{$data[0]}'></td>
                                <td width='80%'><label for='s_{$no}_0'>{$data[1]} </label></td>
                            </tr>
                            <tr>
                                <td><input type='radio' id='s_{$no}_1' name='s[{$no}]' value='{$data[2]}'></td>
                            <td width='80%'><label for='s_{$no}_1'>{$data[3]} </label></td>
                            </tr>";
                    }
                    echo "</table>"; ?><div style="padding:5px;">
                        <input type="hidden" name="siswa_id" value=<?php echo $siswa_id; ?>>
                        <p><input type="button" onclick="trans(-1);" id='prev' class="w3-button w3-round-xlarge w3-blue" disabled="disabled" value="prev"><input type="button" onclick="trans(1);" id='next' class="w3-button w3-round-xlarge w3-blue" value="next"><input type="submit" name="form1" id='submit' class="w3-button w3-round-xlarge w3-green" style='display:none' value="submit"><br></p>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>public/js/util.php?total_page=<?php echo $total_page; ?>"></script>