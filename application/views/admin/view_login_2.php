<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@jiaec">
    <meta name="twitter:creator" content="@jiaec">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="PT JIAEC">
    <meta name="twitter:description" content="Penyelenggara Program Pemagangan ke Jepang">
    <meta name="twitter:image" content="<?php echo base_url(); ?>public/themes/dashforge/img/pt-jiaec-depok.jpg">

    <!-- Facebook -->
    <meta property="og:url" content="http://jiaec.co.id">
    <meta property="og:title" content="PT JIAEC">
    <meta property="og:description" content="Penyelenggara Program Pemagangan ke Jepang">

    <meta property="og:image" content="<?php echo base_url(); ?>public/themes/dashforge/img/pt-jiaec-depok.jpg">
    <meta property="og:image:secure_url" content="<?php echo base_url(); ?>public/themes/dashforge/img/pt-jiaec-depok.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="We Build Our Future">
    <meta name="author" content="PT JIAEC">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>public/uploads/favicon.png">

    <title>Sistem Seleksi Online PT JIAEC</title>

    <!-- vendor css -->
    <link href="<?php echo base_url(); ?>public/themes/dashforge/lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>public/themes/dashforge/lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- DashForge CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/themes/dashforge/css/dashforge.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/themes/dashforge/css/dashforge.auth.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>public/themes/dashforge/css/skin.dark.css">
</head>

<body>

    <div class="content content-fixed content-auth">
        <div class="container-fluid">
            <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
                <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
                    <div class="wd-100p">
                        <!--<h3 class="tx-color-01 mg-b-5">Sign In</h3>
              <p class="tx-color-04 mg-b-10 tx-center">Welcome back! Please signin to continue.</p>-->
                        <div class="card">
                            <div class="card-header df-logo">
                                <img src="<?php echo base_url(); ?>public/themes/dashforge/img/logo-jiaec.svg" style="height:15px"> Seleksi<span>Online</span></a>
                            </div>
                            <div class="card-body pd-20">
                                <?php echo form_open(base_url() . 'admin'); ?>
                                <?php
                                if ($this->session->flashdata('error')) {
                                    echo '<div class="error">' . $this->session->flashdata('error') . '</div>';
                                }
                                if ($this->session->flashdata('success')) {
                                    echo '<div class="success">' . $this->session->flashdata('success') . '</div>';
                                }
                                ?>

                                <div class="form-group df-">
                                    <label>Email address</label>
                                    <input class="form-control" placeholder="Email address" name="email" type="email" autocomplete="off" autofocus>
                                </div>
                                <div class="form-group df-">
                                    <label>Password</label>
                                    <input class="form-control" placeholder="Password" name="password" type="password" autocomplete="off" value="">
                                </div>

                                <button class="btn btn-brand-02 btn-block" type="submit" name="form1">Sign In</button>
                                <!--<div class="divider-text">or</div>
                  <button class="btn btn-outline-facebook btn-block">Sign In With Facebook</button>
                  <button class="btn btn-outline-twitter btn-block">Sign In With Twitter</button>
                  <div class="tx-13 mg-t-20 tx-center">Don't have an account? <a href="page-signup.html">Create an Account</a></div>-->
                                <?php echo form_close(); ?>
                            </div><!-- card body -->
                        </div><!-- card -->
                        <p class="tx-color-04 mg-t-10 tx-center">&#169; PT Japan Indonesian Economic Center</p>
                    </div><!-- wd-100p -->
                </div><!-- sign-wrapper -->
            </div><!-- media -->
        </div><!-- container -->
    </div><!-- content -->

    <script src="<?php echo base_url(); ?>public/themes/dashforge/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/themes/dashforge/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>public/themes/dashforge/lib/feather-icons/feather.min.js"></script>
    <script src="<?php echo base_url(); ?>public/themes/dashforge/lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <script src="<?php echo base_url(); ?>public/themes/dashforge/js/dashforge.js"></script>

</body>

</html>