<?php
if (!$this->session->userdata('id')) {
  redirect(base_url() . 'admin');
}
?>
<section class="content-header">
  <h1>Dashboard</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="info-box">
        <canvas id="myChart" style="width:100%;max-width:100%"></canvas>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script>
      var xValues = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      new Chart("myChart", {
        type: "line",
        data: {
          labels: xValues,
          datasets: [{
            label: "Siswa Seleksi",
            data: [<?php echo implode(",", array_values($siswa_peryear)); ?>],
            borderColor: "red",
            fill: "false"
          }, {
            label: "Siswa Lulus Psikotest",
            data: [<?php echo implode(",", array_values($siswa_psikotest_peryear)); ?>],
            borderColor: "green",
            fill: false
          }, {
            label: "Siswa Papikostik",
            data: [<?php echo implode(",", array_values($siswa_papi_peryear)); ?>],
            borderColor: "blue",
            fill: false
          }, {
            label: "Siswa Lulus Seleksi",
            data: [<?php echo implode(",", array_values($siswa_lulus_peryear)); ?>],
            borderColor: "yellow",
            fill: false
          }]
        },
        options: {
          legend: {
            display: true,
            position: 'right'
          },
          title: {
            display: true,
            text: 'Grafik Data Siswa Tahun <?= date('Y') ?>'
          }
        }
      });
    </script>
  </div>
</section>