<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin Panel</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/datepicker3.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/all.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/select2.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/dataTables.bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/_all-skins.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/summernote.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/admin/css/style.css">
	<style>
		.skin-blue .wrapper,
		.skin-blue .main-header .logo,
		.skin-blue .main-header .navbar,
		.skin-blue .main-sidebar,
		.content-header .content-header-right a,
		.content .form-horizontal .btn-success {
			background-color: #4172a5 !important;
		}

		.content-header .content-header-right a,
		.content .form-horizontal .btn-success {
			border-color: #4172a5 !important;
		}

		.content-header>h1,
		.content-header .content-header-left h1,
		h3 {
			color: #4172a5 !important;
		}

		.box.box-info {
			border-top-color: #4172a5 !important;
		}

		.nav-tabs-custom>.nav-tabs>li.active {
			border-top-color: #f4f4f4 !important;
		}

		.skin-blue .sidebar a {
			color: #fff !important;
		}

		.skin-blue .sidebar-menu>li>.treeview-menu {
			margin: 0 !important;
		}

		.skin-blue .sidebar-menu>li>a {
			border-left: 0 !important;
		}

		.nav-tabs-custom>.nav-tabs>li {
			border-top-width: 1px !important;
		}
	</style>
</head>

<body class="hold-transition fixed skin-blue sidebar-mini">
	<div class="wrapper">
		<header class="main-header">
			<a href="<?php echo base_url(); ?>admin/dashboard" class="logo">
				<span class="logo-lg">PT JIAEC</span>
			</a>
			<nav class="navbar navbar-static-top">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>
				<span style="float:left;line-height:50px;color:#fff;padding-left:15px;font-size:18px;">Admin Panel</span>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li>
							<a href="<?php echo base_url(); ?>" target="_blank">Visit Website</a>
						</li>
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<?php if ($this->session->userdata('photo') == '') : ?>
									<img src="<?php echo base_url(); ?>public/img/no-photo.jpg" class="user-image" alt="user photo">
								<?php else : ?>
									<img src="<?php echo base_url(); ?>public/uploads/<?php echo $this->session->userdata('photo'); ?>" class="user-image" alt="user photo">
								<?php endif; ?>
								<span class="hidden-xs"><?php echo $this->session->userdata('full_name'); ?></span>
							</a>
							<ul class="dropdown-menu">
								<li class="user-footer">
									<div>
										<a href="<?php echo base_url(); ?>admin/profile" class="btn btn-default btn-flat">Edit Profile</a>
									</div>
									<div>
										<a href="<?php echo base_url(); ?>admin/login/logout" class="btn btn-default btn-flat">Log out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<?php
		$class_name = '';
		$segment_2 = 0;
		$segment_3 = 0;
		$class_name = $this->router->fetch_class();
		$segment_2 = $this->uri->segment('2');
		$segment_3 = $this->uri->segment('3');
		$segment_4 = $this->uri->segment('4');
		?>
		<aside class="main-sidebar">
			<section class="sidebar">
				<ul class="sidebar-menu">
					<li class="treeview <?php if ($class_name == 'dashboard') {
											echo 'active';
										} ?>">
						<a href="<?php echo base_url(); ?>admin/dashboard">
							<i class="fa fa-laptop"></i> <span>Dashboard</span>
						</a>
					</li>
					<?php if ($this->session->userdata('role') == 'Admin') : ?>
						<li class="treeview <?php if (($class_name == 'siswa' && $segment_3 == 'pendaftar')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/siswa/pendaftar">
								<i class="fa fa-users"></i> <span>Peserta Seleksi</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'siswa' && $segment_3 == 'siswa_psikotest')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/siswa/siswa_psikotest">
								<i class="fa fa-users"></i> <span>Siswa Lulus Psikotest</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'siswa' && $segment_3 == 'siswa_papikostik')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/siswa/siswa_papikostik">
								<i class="fa fa-users"></i> <span>Siswa Papikostik</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'siswa' && $segment_3 == 'index' && $segment_4 == '1')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/siswa/index/1">
								<i class="fa fa-users"></i> <span>Siswa Lulus Seleksi</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'siswa' && $segment_3 == 'index' && $segment_4 == '2')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/siswa/index/2">
								<i class="fa fa-users"></i> <span>Siswa Tidak Lulus Seleksi</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'soal_psikotest' && $segment_3 == 'index')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/soal_psikotest">
								<i class="fa fa-book"></i> <span>Soal Psikotest</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'soal_papikostik')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/soal_papikostik">
								<i class="fa fa-book"></i> <span>Soal Papikostik</span>
							</a>
						</li>
						<li class="treeview <?php if (($class_name == 'user')) {
												echo 'active';
											} ?>">
							<a href="<?php echo base_url(); ?>admin/user">
								<i class="fa fa-user-circle"></i> <span>User</span>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</section>
		</aside>
		<div class="content-wrapper">