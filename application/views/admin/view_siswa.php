<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>View Siswa <?= $status; ?> Seleksi</h1>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
            ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
            <?php
            }
            if ($this->session->flashdata('success')) {
            ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
            <?php
            }
            ?>
            <div class="box box-info">
                <div class="box-header">
                    <div class="box-title">
                        <?php
                        $url = base_url() . 'admin/siswa/report/' . $status_id;
                        ?>
                        <a class="btn btn-primary btn-xs" href="<?= $url; ?>"><i class="fa fa-file"></i> Report</a>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Sekolah</th>
                                    <th>Jurusan</th>
                                    <th>Tanggal Seleksi</th>
                                    <th>Interviewer</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($siswa as $row) {
                                    $i++;
                                ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['nama_lengkap']; ?></td>
                                        <td><?php echo $row['asal_sekolah']; ?></td>
                                        <td><?php echo $row['jurusan']; ?></td>
                                        <td><?php echo $row['tanggal_seleksi']; ?></td>
                                        <td><?php echo $row['interviewer']; ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>admin/siswa/detail_siswa/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">View</a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>