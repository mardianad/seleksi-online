<?php
if (!$this->session->userdata('id')) {
	redirect(base_url() . 'admin');
}
?>
<section class="content-header">
	<div class="content-header-left">
		<h1>Add Soal Psikotest</h1>
	</div>
	<div class="content-header-right">
		<a href="<?php echo base_url(); ?>admin/soal_psikotest" class="btn btn-primary btn-sm">View All</a>
	</div>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<?php
			if ($this->session->flashdata('error')) {
			?>
				<div class="callout callout-danger">
					<p><?php echo $this->session->flashdata('error'); ?></p>
				</div>
			<?php
			}
			if ($this->session->flashdata('success')) {
			?>
				<div class="callout callout-success">
					<p><?php echo $this->session->flashdata('success'); ?></p>
				</div>
			<?php
			}
			?>
			<?php echo form_open_multipart(base_url() . 'admin/soal_psikotest/add', array('class' => 'form-horizontal')); ?>
			<div class="box box-info">
				<div class="box-body">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Bab <span>*</span></label>
						<div class="col-sm-3">
							<select class="form-control select2" name="bab_id">
								<option value="">Select a bab</option>
								<?php
								$i = 0;
								foreach ($bab as $row) {
								?>
									<option value="<?php echo $row['id']; ?>"><?php echo $row['bab']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Soal <span>*</span></label>
						<div class="col-sm-9">
							<textarea class="form-control editor" name="soal"><?php if (isset($_POST['soal'])) {
																					echo $_POST['soal'];
																				} ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Photo</label>
						<div class="col-sm-6" style="padding-top:6px;">
							<input type="file" name="photo">
						</div>
					</div>

					<h3 class="seo-info">Pilihan Jawaban</h3>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Jawaban A </label>
						<div class="col-sm-6">
							<textarea class="form-control editor" name="a_jawaban"><?php if (isset($_POST['a_jawaban'])) {
																						echo $_POST['a_jawaban'];
																					} ?></textarea>
						</div>
						<label for="" class="col-sm-1 control-label">Photo A</label>
						<div class="col-sm-2" style="padding-top:6px;">
							<input type="file" name="a_gambar">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Jawaban B </label>
						<div class="col-sm-6">
							<textarea class="form-control editor" name="b_jawaban"><?php if (isset($_POST['b_jawaban'])) {
																						echo $_POST['b_jawaban'];
																					} ?></textarea>
						</div>
						<label for="" class="col-sm-1 control-label">Photo B</label>
						<div class="col-sm-2" style="padding-top:6px;">
							<input type="file" name="b_gambar">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Jawaban C </label>
						<div class="col-sm-6">
							<textarea class="form-control editor" name="a_jawaban"><?php if (isset($_POST['c_jawaban'])) {
																						echo $_POST['c_jawaban'];
																					} ?></textarea>
						</div>
						<label for="" class="col-sm-1 control-label">Photo C</label>
						<div class="col-sm-2" style="padding-top:6px;">
							<input type="file" name="c_gambar">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Jawaban D </label>
						<div class="col-sm-6">
							<textarea class="form-control editor" name="d_jawaban"><?php if (isset($_POST['d_jawaban'])) {
																						echo $_POST['d_jawaban'];
																					} ?></textarea>
						</div>
						<label for="" class="col-sm-1 control-label">Photo D</label>
						<div class="col-sm-2" style="padding-top:6px;">
							<input type="file" name="d_gambar">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Jawaban E </label>
						<div class="col-sm-6">
							<textarea class="form-control editor" name="d_jawaban"><?php if (isset($_POST['e_jawaban'])) {
																						echo $_POST['e_jawaban'];
																					} ?></textarea>
						</div>
						<label for="" class="col-sm-1 control-label">Photo E</label>
						<div class="col-sm-2" style="padding-top:6px;">
							<input type="file" name="e_gambar">
						</div>
					</div>
					<h3 class="seo-info">Kunci Jawaban</h3>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Kunci Jawaban <span>*</span></label>
						<div class="col-sm-3">
							<select class="form-control select2" name="jawaban">
								<option disabled>Select One</option>
								<option value="A">A</option>
								<option value="B">B</option>
								<option value="C">C</option>
								<option value="D">D</option>
								<option value="E">E</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label"></label>
						<div class="col-sm-6">
							<button type="submit" class="btn btn-success pull-left" name="form1">Submit</button>
						</div>
					</div>
				</div>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</section>