<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>Result Papikostik</h1>
    </div>
</section>
<section class="content" style="min-height:auto;margin-bottom: -30px;">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
            ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
            <?php
            }
            if ($this->session->flashdata('success')) {
            ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">PAPIKOSTICK</a></li>
                    <li><a href="<?= base_url() . 'admin/siswa/siswa_result_papikostik/' . $siswa_id . '/report' ?>"> <i class="fa fa-download"></i> Report</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <h2><b><?= $siswa['nama_lengkap']; ?></b> - Interprestation Result</h2>
                        <br />
                        <table id="example1" class="table table-bordered table-striped">
                            <tr>
                                <th>No</th>
                                <th>ASPECT</th>
                                <th>ROLE</th>
                                <th>RESULT</th>
                            </tr>
                            <?php
                            $aspect = '';
                            $no = 0;
                            foreach ($papi_result as $r) {
                                if ($aspect != $r['aspect']) {
                                    $aspect = $r['aspect'];
                                    echo "<tr><td>" . (++$no) . "</td><td colspan='3'><b>{$aspect}</b></td></tr>";
                                }
                                echo "<tr><td></td><td>&nbsp;</td><td colspan='2'>{$r['role']}</td></tr><tr><td></td><td>&nbsp</td><td>&nbsp</td><td>-{$r['interprestation']}</td></tr>";
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>