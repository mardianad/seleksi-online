<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>View Soal Papikostik</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/soal_papikostik/add" class="btn btn-primary btn-sm">Add New</a>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
            ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
            <?php
            }
            if ($this->session->flashdata('success')) {
            ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
            <?php
            }
            ?>
            <div class="box box-info">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Pertanyaan 1</th>
                                <th>Nilai 1</th>
                                <th>Pertanyaan 2</th>
                                <th>Nilai 2</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 0;
                            foreach ($papi as $row) {
                                $i++;
                            ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $row['question1']; ?></td>
                                    <td><?php echo $row['value1']; ?></td>
                                    <td><?php echo $row['question2']; ?></td>
                                    <td><?php echo $row['value2']; ?></td>

                                    <td>
                                        <a href="<?php echo base_url(); ?>admin/soal_papikostik/edit/<?php echo $row['id']; ?>" class="btn btn-primary btn-xs">Edit</a>
                                        <a href="<?php echo base_url(); ?>admin/soal_papikostik/delete/<?php echo $row['id']; ?>" class="btn btn-danger btn-xs" onClick="return confirm('Are you sure?');">Delete</a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>