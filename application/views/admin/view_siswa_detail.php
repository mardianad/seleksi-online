<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>Detail Siswa</h1>
    </div>
</section>
<section class="content" style="min-height:auto;margin-bottom: -30px;">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
            ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
            <?php
            }
            if ($this->session->flashdata('success')) {
            ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Detail Siswa</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Persyaratan Siswa</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Nilai Psikotest</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/siswa/siswa_result_papikostik/<?php echo $siswa['id']; ?>">Result Papikostick</a></li>
                </ul>
                <div class="tab-content">
                    <h2><b><?= $siswa['nama_lengkap'] ?></b></h2>
                    <br />
                    <div class="tab-pane active" id="tab_1">
                        <div class="panel panel-default">
                            <div class="panel-heading">Data Diri</div>
                            <div class="panel-body">
                                <div class="form-row row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama_lengkap" value="<?= $siswa['nama_lengkap'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Panggilan</label>
                                            <input type="text" class="form-control" placeholder="Nama Panggilan" name="nama_panggilan" value="<?= $siswa['nama_panggilan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>NIK</label>
                                            <input type="text" class="form-control" placeholder="NIK" name="no_ktp" value="<?= $siswa['no_ktp'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Sekolah</label>
                                            <input type="text" class="form-control" value="<?= $siswa['asal_sekolah'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Jurusan</label>
                                            <input type="text" class="form-control" value="<?= $siswa['jurusan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Tahun Lulus</label>
                                            <input type="text" class="form-control" name="tahun_lulus" value="<?= $siswa['tahun_lulus'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tempat Lahir</label>
                                            <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" value="<?= $siswa['tempat_lahir'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tanggal Lahir</label>
                                            <input type="text" class="form-control" name="tanggal_lahir" value="<?= $siswa['tanggal_lahir'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Jenis Kelamin</label>
                                            <input type="text" class="form-control" value="<?= $siswa['jenis_kelamin'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Agama</label>
                                            <input type="text" class="form-control" value="<?= $siswa['agama'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Suku</label>
                                            <input type="text" class="form-control" placeholder="Suku" name="suku" value="<?= $siswa['suku'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>No Telepon/HP</label>
                                            <input name="mobile_phone" type="text" class="form-control" value="<?= $siswa['mobile_phone'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>No Telepon/HP Lainnya</label>
                                            <input name="telephone" type="text" class="form-control" value="<?= $siswa['telephone'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input name="email" type="email" class="form-control" value="" placeholder="Email" value="<?= $siswa['email'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">Alamat</div>
                            <div class="panel-body">
                                <div class="form-row row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Nama Jalan</label>
                                            <input type="text" class="form-control" placeholder="Nama Jalan/Gang/Blok" name="nama_jalan" value="<?= $siswa['nama_jalan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>No Rumah</label>
                                            <input type="text" class="form-control" placeholder="No Rumah" name="no_rumah" value="<?= $siswa['no_rumah'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>RT</label>
                                            <input type="text" class="form-control" placeholder="RT" name="rt" value="<?= $siswa['rt'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>RW</label>
                                            <input type="text" class="form-control" placeholder="RW" name="rw" value="<?= $siswa['rw'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kelurahan</label>
                                            <input type="text" class="form-control" placeholder="Kelurahan" name="kelurahan" value="<?= $siswa['kelurahan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kecamatan</label>
                                            <input type="text" class="form-control" placeholder="Kecamatan" name="kecamatan" value="<?= $siswa['kecamatan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kota</label>
                                            <input type="text" class="form-control" placeholder="Kota" name="kota" value="<?= $siswa['kota'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Provinsi</label>
                                            <input type="text" class="form-control" placeholder="Provinsi" name="provinsi" value="<?= $siswa['provinsi'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">Fisik</div>
                            <div class="panel-body">
                                <div class="form-row row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Tinggi Badan</label>
                                            <input type="number" class="form-control" placeholder="Tinggi Badan" name="tinggi_badan" value="<?= $siswa['tinggi_badan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Berat Badan</label>
                                            <input type="number" class="form-control" placeholder="Berat Badan" name="berat_badan" value="<?= $siswa['berat_badan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mata Kanan</label>
                                            <input type="text" class="form-control" placeholder="Mata Kanan" name="mata_kanan" value="<?= $siswa['mata_kanan'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Mata Kiri</label>
                                            <input type="text" class="form-control" placeholder="Mata Kiri" name="mata_kiri" value="<?= $siswa['mata_kiri'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Merokok</label>
                                            <input type="text" class="form-control" value="<?= $siswa['merokok'] ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Status Pernikahan</label>
                                            <input type="text" class="form-control" value="<?= $siswa['status_pernikahan'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_2">
                        <div class="panel panel-default">
                            <div class="panel-heading">Persyaratan</div>
                            <div class="panel-body">
                                <div class="form-row row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Pas Photo</label>
                                            <img src="<?= base_url() ?>public/uploads/siswa/pas_photo/<?= $siswa['foto'] ?>" class="existing-photo" style="height:400px;">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Akta Kelahiran</label>
                                            <br />
                                            <a href="<?= base_url() ?>public/uploads/siswa/akta/<?= $siswa['fc_akta_kelahiran'] ?>"><i class="fa fa-file fa-2x"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Kartu Keluarga</label>
                                            <br />
                                            <a href="<?= base_url() ?>public/uploads/siswa/kk/<?= $siswa['fc_kk'] ?>"><i class="fa fa-file fa-2x"></i></a>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Ijazah / SKL</label>
                                            <br />
                                            <a href="<?= base_url() ?>public/uploads/siswa/ijazah/<?= $siswa['fc_ijazah_smk'] ?>"><i class="fa fa-file fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_3">
                        <div class="panel panel-default">
                            <div class="panel-heading">Nilai Psikotest</div>
                            <div class="panel-body">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <td>BAB</td>
                                        <td>Nilai</td>
                                    </tr>
                                    <?php foreach ($psikotest as $n) { ?>
                                        <tr>
                                            <td><?= $n['bab'] ?></td>
                                            <td><?= $n['nilai'] ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>