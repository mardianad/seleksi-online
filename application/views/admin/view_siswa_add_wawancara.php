<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Nilai Interview Siswa</h1>
    </div>
</section>

<section class="content" style="min-height:auto;margin-bottom: -30px;">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
            ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
            <?php
            }
            if ($this->session->flashdata('success')) {
            ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12">

            <div class="nav-tabs-custom">

                <div class="tab-content">
                    <?php
                    if (!empty($nilai)) {
                        $sikap = $nilai['nilai_sikap'];
                        $sifat = $nilai['nilai_sifat'];
                        $komunikasi = $nilai['nilai_komunikasi'];
                    } else {
                        $sikap = "";
                        $sifat = "";
                        $komunikasi = "";
                    }
                    ?>
                    <h2><b><?= $siswa['nama_lengkap'] ?></b></h2>
                    <br />
                    <div class="panel panel-default">
                        <div class="panel-heading">Data Wawancara</div>
                        <div class="panel-body">
                            <?php echo form_open_multipart(base_url() . 'admin/siswa/wawancara/' . $siswa['id']); ?>
                            <div class="form-row row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Tanggal Wawancara</label>
                                        <input type="text" class="form-control" name="tanggal_wawancara" id="datepicker" value="<?php echo $siswa['tanggal_wawancara'] ?>" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nilai Sikap</label>
                                        <input type="text" class="form-control" name="nilai_sikap" value="<?php echo $sikap ?>" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nilai Sifat</label>
                                        <input type="text" class="form-control" name="nilai_sifat" value="<?php echo $sifat ?>" required>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nilai Kemampuan Komunikasi</label>
                                        <input type="text" class="form-control" name="nilai_komunikasi" value="<?php echo $komunikasi ?>" required>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Catatan</label>
                                        <textarea name="catatan" class="form-control editor"><?= $siswa['catatan'] ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group col-12">
                                    <button type="submit" class="btn btn-fill btn-primary" name="form1">Submit</button>
                                </div>
                                <?php echo form_close(); ?>

                            </div>
                        </div>
                    </div>





                </div>
            </div>


        </div>
    </div>

</section>