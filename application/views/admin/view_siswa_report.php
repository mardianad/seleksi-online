<?php
$name = "LaporanSiswaLulus";
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$name.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="0">
    <tr>
        <td colspan="6" heigth="100px">
            <img src="<?php echo base_url(); ?>public/uploads/favicon-logo.png" style="width:20px;">
            <b>
                <font size="18px">JAPAN INDONESIAN ECONOMIC CENTER</font>
            </b>
        </td>
    </tr>
</table>
<table border='1'>
    <thead>
        <tr>
            <td width="2%">No</td>
            <td>Nama Lengkap</td>
            <td>Nama Panggilan</td>
            <td>Sekolah</td>
            <td>Jurusan</td>
            <td>Tanggal Seleksi</td>
        </tr>
    </thead>
    <tbody>
        <?php $i = 1;
        foreach ($siswa as $row) { ?>
            <tr>
                <td align="center"><?php echo $i; ?></td>
                <td><?php echo $row['nama_lengkap']; ?></td>
                <td><?php echo $row['nama_panggilan']; ?></td>
                <td><?php echo $row['asal_sekolah']; ?></td>
                <td><?php echo $row['jurusan']; ?></td>
                <td><?php echo $row['tanggal_seleksi']; ?></td>
            </tr>
        <?php $i++;
        } ?>
    </tbody>
</table>