<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<?php echo form_open_multipart(base_url() . 'admin/siswa/report_psikotest/', array('class' => 'form-horizontal')); ?>
<section class="content-header">
    <div class="content-header-left">
        <h1>Laporan Siswa Psikotest</h1>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
            ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
            <?php
            }
            if ($this->session->flashdata('success')) {
            ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
            <?php
            }
            ?>
            <div class="box box-info">
                <div class="box-header">
                    <div class="box-title">
                        <a class="btn btn-info btn-xs" data-toggle="collapse" href="#btnfilter"><i class="fa fa-filter"></i> Filter</a>
                        <?php
                        $conditions = str_replace("'", "%27", $fconditions);
                        $url = base_url() . 'admin/siswa/report_psikotest/download/' . $conditions;
                        ?>
                        <a class="btn btn-primary btn-xs" href="<?= $url; ?>"><i class="fa fa-download"></i> Download</a>
                    </div>
                    <div class="box-body table-responsive">
                        <div id="btnfilter" class="collapse">
                            <div class="container-fluid">
                                <div class="row border bg-info">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Dari Tanggal</label>
                                            <input type="text" name="tanggal_seleksi[]" class="form-control" id="datepicker1">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Sampai Tanggal</label>
                                            <input type="text" name="tanggal_seleksi[]" class="form-control" id="datepicker">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-default btn-sm" name="filter">Filter</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Sekolah</th>
                                    <th>Jurusan</th>
                                    <th>Tanggal Seleksi</th>
                                    <th>Total Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($siswa as $row) {
                                    $i++;
                                ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['nama_lengkap']; ?></td>
                                        <td><?php echo $row['asal_sekolah']; ?></td>
                                        <td><?php echo $row['jurusan']; ?></td>
                                        <td><?php echo $row['tanggal_seleksi']; ?></td>
                                        <td><?php echo $row['total_nilai']; ?></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo form_close(); ?>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>