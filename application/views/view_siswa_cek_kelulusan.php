<div class="content-wrapper text-center">
    <div class="content">
        <div class="container my-auto">
            <div class="row pt-5">
                <div class="col-12 mx-auto">
                    <img src="" />
                    <img src="<?= base_url('public/uploads/jiaec-img.png'); ?>" width="250" height="250" />
                    <br /> <br />
                    <h1 class="display-4 pt-3">INFORMASI KELULUSAN </h1>
                    <br />
                    <h3>Tes Seleksi Pemagang ke Jepang</h3>
                    <br />
                    <hr />
                </div>
            </div>
            <br /> <br />
            <div class="row pt-2">
                <div class="col-12 col-lg-8 mx-auto">
                    <h2><b>HASIL SELEKSI</b></h2>
                    <br />
                    <?php
                    if ($status == 0) {
                        echo "<h1> MAAF! </h1><br/> <h2> Data yang anda masukkan tidak ada di dalam data kami. <br/> Silahkan periksa kembali data anda!</h2>";
                    } else {
                    ?>
                        <center>
                            <table style="width:100%">
                                <tr>
                                    <td width="20%" height="40">Nama</td>
                                    <td>:</td>
                                    <td style="white-space:nowrap"><?= $siswa['nama_lengkap']; ?></td>
                                </tr>
                                <tr>
                                    <td width="20%" height="40">Tanggal Lahir</td>
                                    <td>:</td>
                                    <td style="white-space:nowrap"><?= $siswa['tanggal_lahir'] ?></td>
                                </tr>
                                <tr>
                                    <td width="20%" height="40">Sekolah</td>
                                    <td>:</td>
                                    <td style="white-space:nowrap"><?= $siswa['asal_sekolah'] ?></td>
                                </tr>
                                <tr>
                                    <td width="20%" height="40">Jurusan</td>
                                    <td>:</td>
                                    <td style="white-space:nowrap"><?= $siswa['jurusan'] ?></td>
                                </tr>
                                <tr>
                                    <td width="20%" height="40">Tanggal Seleksi</td>
                                    <td>:</td>
                                    <td style="white-space:nowrap"><?= $siswa['tanggal_seleksi'] ?></td>
                                </tr>
                                <tr>
                                    <td width="20%" height="40">Status</td>
                                    <td>:</td>
                                    <td style="white-space:nowrap"><?= $siswa['status_identitas_seleksi'] ?></td>
                                </tr>
                                <?php if ($siswa['status_identitas_seleksi'] == "Lulus Seleksi") { ?>
                                    <tr>
                                        <td width="20%" height="40">Tanggal MCU 1</td>
                                        <td>:</td>
                                        <td><?= $siswa['tanggal_mcu_1'] ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </center>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>