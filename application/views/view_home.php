<div class="slider">
    <div class="slide-carousel slider-one owl-carousel">
        <?php
        foreach ($sliders as $slider) {
        ?>
            <div class="slider-item flex" style="background-image:url(<?php echo base_url(); ?>public/uploads/<?php echo $slider['photo']; ?>);">
                <div class="bg-slider"></div>
                <div class="container">
                    <div class="row">
                        <div class="<?php if ($slider['position'] == 'Left') {
                                        echo 'col-lg-6 col-md-9 col-12';
                                    } else {
                                        echo 'offset-lg-6 col-lg-6 offset-md-3 col-md-9 col-12';
                                    } ?>">
                            <div class="slider-text">

                                <?php if ($slider['heading'] != '') : ?>
                                    <div class="text-animated">
                                        <h1><?php echo $slider['heading']; ?></h1>
                                    </div>
                                <?php endif; ?>

                                <?php if ($slider['content'] != '') : ?>
                                    <div class="text-animated">
                                        <p>
                                            <?php echo nl2br($slider['content']); ?>
                                        </p>
                                    </div>
                                <?php endif; ?>


                                <?php if ($slider['button1_text'] != '' || $slider['button2_text'] != '') : ?>
                                    <div class="text-animated">
                                        <ul>
                                            <?php if ($slider['button1_text'] != '') : ?>
                                                <li><a href="<?php echo $slider['button1_url']; ?>"><?php echo $slider['button1_text']; ?></a></li>
                                            <?php endif; ?>

                                            <?php if ($slider['button2_text'] != '') : ?>
                                                <li><a href="<?php echo $slider['button2_url']; ?>"><?php echo $slider['button2_text']; ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>
<?php if ($page_home['home_welcome_status'] == 'Show') : ?>
    <div class="about-area pt_60 pb_90">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mt_30">
                    <div class="about-content">
                        <div class="headline-left">
                            <h2>
                                <span><?php echo $page_home['home_welcome_title']; ?></span> <?php echo $page_home['home_welcome_subtitle']; ?>
                            </h2>
                        </div>
                        <p>
                            <?php echo $page_home['home_welcome_text']; ?>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 mt_30">
                    <div class="about-tab" style="background-image: url(<?php echo base_url(); ?>public/uploads/slider-1.jpg)">
                        <div class="video-section">
                            <a class="video-button" href="#" data-toggle="modal" data-target="#myModal"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="video-modal">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="video-box bg-area">
                            <div class="modal fade in" id="myModal" role="dialog">
                                <div class="modal-dialog hb-style">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <video width="100%" height="auto" controls>
                                                <source src="<?php echo base_url(); ?>public/uploads/<?= $page_home['home_welcome_video']; ?>" type="video/mp4">
                                            </video>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if ($page_home['counter_status'] == 'Show') : ?>
    <div class="counterup-area pt_60 pb_90" style="background-image: url(<?php echo base_url(); ?>public/uploads/<?php echo $page_home['counter_photo']; ?>)">
        <div class="bg-counterup"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="counter-item flex">
                        <i class="<?php echo $page_home['counter_1_icon']; ?>" aria-hidden="true"></i>
                        <h2 class="counter"><?php echo $page_home['counter_1_value']; ?></h2>
                        <h4><?php echo $page_home['counter_1_title']; ?></h4>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="counter-item flex">
                        <i class="<?php echo $page_home['counter_2_icon']; ?>" aria-hidden="true"></i>
                        <h2 class="counter"><?php echo $page_home['counter_2_value']; ?></h2>
                        <h4><?php echo $page_home['counter_2_title']; ?></h4>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php endif; ?>