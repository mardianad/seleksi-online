<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<div class="faq-area">
    <div class="container">
        <center>
            <h2>Pendaftaran</h2>
        </center>
        <br />
        <?php echo form_open_multipart(base_url() . 'siswa/pendaftaran/'); ?>
        <div class="panel panel-default">
            <div class="panel-heading">Data Diri</div>
            <div class="panel-body">
                <div class="form-row row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama_lengkap" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nama Panggilan</label>
                            <input type="text" class="form-control" placeholder="Nama Panggilan" name="nama_panggilan" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>NIK</label>
                            <input type="text" class="form-control" placeholder="NIK" name="no_ktp" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Sekolah</label>
                            <select class="form-control select2" name="asal_sekolah">
                                <option value="">Choose One</option>
                                <?php
                                foreach ($sekolah as $row) {
                                ?>
                                    <option value="<?php echo $row['title']; ?>"><?php echo $row['title']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Jurusan</label>
                            <select class="form-control select2" name="jurusan">
                                <option value="">Choose One</option>
                                <?php
                                foreach ($jurusan as $j) {
                                ?>
                                    <option value="<?php echo $j['title']; ?>"><?php echo $j['title']; ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Tahun Lulus</label>
                            <input type="text" class="form-control datepickerYear" name="tahun_lulus" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" class="form-control" id="datepicker" name="tanggal_lahir" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <select class="form-control select2" name="jenis_kelamin">
                                <option value="">Choose One</option>
                                <option value="Laki-Laki"> Laki-Laki</option>
                                <option value="Perempuan"> Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Agama</label>
                            <select class="form-control select2" name="agama">
                                <option value="">Choose One</option>
                                <option value="Islam"> Islam</option>
                                <option value="Kristen"> Kristen</option>
                                <option value="Katolik"> Katolik</option>
                                <option value="Hindu"> Hindu</option>
                                <option value="Budha"> Budha</option>
                                <option value="Konghucu"> Konghucu</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Suku</label>
                            <input type="text" class="form-control" placeholder="Suku" name="suku" required>
                        </div>
                    </div>


                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>No Telepon/HP</label>
                            <input name="mobile_phone" type="text" class="form-control" value="" placeholder="No Telepon/HP" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>No Telepon/HP Lainnya</label>
                            <input name="telephone" type="text" class="form-control" value="" placeholder="No Telepon/HP">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input name="email" type="email" class="form-control" value="" placeholder="Email" required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Alamat</div>
            <div class="panel-body">
                <div class="form-row row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Nama Jalan</label>
                            <input type="text" class="form-control" placeholder="Nama Jalan/Gang/Blok" name="nama_jalan" required>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>No Rumah</label>
                            <input type="text" class="form-control" placeholder="No Rumah" name="no_rumah">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>RT</label>
                            <input type="text" class="form-control" placeholder="RT" name="rt" required>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>RW</label>
                            <input type="text" class="form-control" placeholder="RW" name="rw" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Kelurahan</label>
                            <input type="text" class="form-control" placeholder="Kelurahan" name="kelurahan" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <input type="text" class="form-control" placeholder="Kecamatan" name="kecamatan" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Kota</label>
                            <input type="text" class="form-control" placeholder="Kota" name="kota" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Provinsi</label>
                            <input type="text" class="form-control" placeholder="Provinsi" name="provinsi" required>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Fisik</div>
            <div class="panel-body">
                <div class="form-row row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tinggi Badan</label>
                            <input type="number" class="form-control" placeholder="Tinggi Badan" name="tinggi_badan" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Berat Badan</label>
                            <input type="number" class="form-control" placeholder="Berat Badan" name="berat_badan" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Mata Kanan</label>
                            <input type="text" class="form-control" placeholder="Mata Kanan" name="mata_kanan" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Mata Kiri</label>
                            <input type="text" class="form-control" placeholder="Mata Kiri" name="mata_kiri" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Merokok</label>
                            <select class="form-control select2" name="merokok">
                                <option value="">Choose One</option>
                                <option value="Ya"> Ya</option>
                                <option value="Tidak"> Tidak</option>
                                <option value="Pernah"> Pernah</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Status Pernikahan</label>
                            <select class="form-control select2" name="status_pernikahan">
                                <option value="">Choose One</option>
                                <option value="Tidak"> Tidak Menikah</option>
                                <option value="Ya"> Menikah</option>
                                <option value="Pernah"> Pernah Menikah</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Persyaratan</div>
            <div class="panel-body">
                <div class="form-row row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Pas Photo</label>
                            <input type="file" class="form-control" name="foto" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Akta Kelahiran</label>
                            <input type="file" class="form-control" name="fc_akta_kelahiran" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Kartu Keluarga</label>
                            <input type="file" class="form-control" name="fc_kk" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Ijazah / SKL</label>
                            <input type="file" class="form-control" name="fc_ijazah_smk">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group col-12">
            <button type="submit" class="btn btn-fill btn-primary" name="form1" onclick="warning_before_save()">Submit</button>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>