<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_psikotest extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_jawaban_psikotest'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_bab_limit()
    {
        $sql = "SELECT * FROM tbl_bab_psikotest ORDER BY id ASC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function check_bab($id)
    {
        $sql = "SELECT * FROM tbl_bab_psikotest WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function show_soal($bab_id)
    {
        $sql = "SELECT * FROM tbl_soal_psikotest WHERE bab_id=$bab_id ORDER BY tbl_soal_psikotest.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_pilihan_soal($id)
    {
        $sql = "SELECT * FROM tbl_soal_psikotest_kj WHERE soal_id=$id ORDER BY tbl_soal_psikotest_kj.id ASC";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function show_nilai($siswa_id)
    {
        $sql = "SELECT SUM(nilai) as total_nilai FROM tbl_jawaban_psikotest WHERE siswa_id=$siswa_id";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function check_kj($id)
    {
        $sql = "SELECT * FROM tbl_soal_psikotest WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function add($data)
    {
        $this->db->insert('tbl_jawaban_psikotest', $data);
        return $this->db->insert_id();
    }
    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_jawaban_psikotest', $data);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_jawaban_psikotest');
    }
    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_jawaban_psikotest WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}
