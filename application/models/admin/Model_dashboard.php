<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_dashboard extends CI_Model
{
    public function siswa_peryear($y)
    {
        $sql = "SELECT month(tanggal_seleksi) as bulan, count(*) as jumlah from tbl_siswa WHERE year(tanggal_seleksi)='$y' GROUP BY month(tanggal_seleksi)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function siswa_psikotest_peryear($y)
    {
        $sql = "SELECT month(tanggal_seleksi) as bulan, count(*) as jumlah from tbl_siswa WHERE year(tanggal_seleksi)='$y' AND nilai_psikotest > 250 GROUP BY month(tanggal_seleksi)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function siswa_papi_peryear($y)
    {
        $sql = "SELECT month(tanggal_seleksi) as bulan, count(*) as jumlah from tbl_siswa WHERE year(tanggal_seleksi)='$y' AND id IN (select siswa_id from tbl_papi_results WHERE tbl_papi_results.siswa_id=tbl_siswa.id) GROUP BY month(tanggal_seleksi)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function siswa_lulus_peryear($y)
    {
        $sql = "SELECT month(tanggal_seleksi) as bulan, count(*) as jumlah from tbl_siswa WHERE year(tanggal_seleksi)='$y' AND (status_identitas_seleksi='Lulus Seleksi' OR status_identitas_seleksi='Stok Rekrut') GROUP BY month(tanggal_seleksi)";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
