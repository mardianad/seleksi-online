<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_siswa extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_siswa'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show($limit = null, $offset = null)
    {
        $query = $this->db->get('tbl_siswa', $limit, $offset);
        return $query->result_array();
    }
    function showCount()
    {
        $sql = "SELECT * FROM tbl_siswa";
        $query = $this->db->query($sql);
        return $query->num_rows();
    }
    function show_siswa_lulus()
    {
        $sql = "SELECT * FROM tbl_siswa WHERE status_identitas_seleksi='Lulus Seleksi'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_siswa_by_status($status)
    {
        $sql = "SELECT * FROM tbl_siswa WHERE status_identitas_seleksi='$status'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function showByFilter($status, $fconditions)
    {
        $sql = "SELECT * FROM tbl_siswa WHERE status_identitas_seleksi='$status'" . $fconditions;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function add($data)
    {
        $this->db->insert('tbl_siswa', $data);
        return $this->db->insert_id();
    }
    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_siswa', $data);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_siswa');
    }
    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_siswa WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
    function getDataKelulusan($nik, $tgl_seleksi)
    {
        $sql = "SELECT * FROM tbl_siswa WHERE no_ktp='$nik' AND tanggal_seleksi='$tgl_seleksi'";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function get_sekolah()
    {
        $sql = "SELECT * FROM tbl_sekolah ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function siswa_check($id)
    {
        $sql = 'SELECT * FROM tbl_siswa WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
    function showByFilter_psikotest_lulus($fconditions)
    {
        $sql = "SELECT tbl_jawaban_psikotest.*, SUM(tbl_jawaban_psikotest.nilai) as total_nilai, tbl_siswa.nama_lengkap, tbl_siswa.nama_panggilan, tbl_siswa.asal_sekolah, tbl_siswa.jurusan, tbl_siswa.tanggal_seleksi FROM tbl_jawaban_psikotest INNER JOIN tbl_siswa ON tbl_jawaban_psikotest.siswa_id=tbl_siswa.id" . $fconditions . " GROUP BY tbl_jawaban_psikotest.siswa_id HAVING SUM(tbl_jawaban_psikotest.nilai) > 150";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function showByFilter_psikotest_tidak_lulus($fconditions)
    {
        $sql = "SELECT tbl_jawaban_psikotest.*, SUM(tbl_jawaban_psikotest.nilai) as total_nilai, tbl_siswa.nama_lengkap, tbl_siswa.nama_panggilan, tbl_siswa.asal_sekolah, tbl_siswa.jurusan, tbl_siswa.tanggal_seleksi FROM tbl_jawaban_psikotest INNER JOIN tbl_siswa ON tbl_jawaban_psikotest.siswa_id=tbl_siswa.id" . $fconditions . " GROUP BY tbl_jawaban_psikotest.siswa_id HAVING SUM(tbl_jawaban_psikotest.nilai) <= 150";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_siswa_lulus_psikotest()
    {
        $sql = "SELECT tbl_jawaban_psikotest.*, SUM(tbl_jawaban_psikotest.nilai) as total_nilai, tbl_siswa.nama_lengkap, tbl_siswa.nama_panggilan, tbl_siswa.asal_sekolah, tbl_siswa.jurusan, tbl_siswa.tanggal_seleksi FROM tbl_jawaban_psikotest INNER JOIN tbl_siswa ON tbl_jawaban_psikotest.siswa_id=tbl_siswa.id GROUP BY tbl_jawaban_psikotest.siswa_id HAVING SUM(tbl_jawaban_psikotest.nilai) > 150";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function cari_siswa($params = array())
    {
        $this->db->select('tbl_jawaban_psikotest.*, SUM(tbl_jawaban_psikotest.nilai) as total_nilai, tbl_siswa.nama_lengkap, tbl_siswa.nama_panggilan, tbl_siswa.asal_sekolah, tbl_siswa.jurusan, tbl_siswa.tanggal_seleksi');
        $this->db->from('tbl_jawaban_psikotest');
        $this->db->join('tbl_siswa', 'tbl_jawaban_psikotest.siswa_id = tbl_siswa.id', 'inner');
        if (!empty($params['search']['keywords'])) {
            $this->db->like('nama', $params['search']['keywords']);
        }
        $this->db->group_by('tbl_jawaban_psikotest.siswa_id HAVING SUM(tbl_jawaban_psikotest.nilai) > 150');
        $query = $this->db->get();
        //return fetched data
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
    function show_siswa_tidak_lulus_psikotest()
    {
        $sql = "SELECT tbl_jawaban_psikotest.*, SUM(tbl_jawaban_psikotest.nilai) as total_nilai, tbl_siswa.nama_lengkap, tbl_siswa.nama_panggilan, tbl_siswa.asal_sekolah, tbl_siswa.jurusan, tbl_siswa.tanggal_seleksi FROM tbl_jawaban_psikotest INNER JOIN tbl_siswa ON tbl_jawaban_psikotest.siswa_id=tbl_siswa.id GROUP BY tbl_jawaban_psikotest.siswa_id HAVING SUM(tbl_jawaban_psikotest.nilai) <= 150";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_papikostik()
    {
        $sql = "SELECT tbl_papi_results.*, tbl_siswa.nama_lengkap, tbl_siswa.asal_sekolah, tbl_siswa.jurusan, tbl_siswa.tanggal_seleksi FROM tbl_papi_results INNER JOIN tbl_siswa ON tbl_papi_results.siswa_id=tbl_siswa.id GROUP BY tbl_papi_results.siswa_id ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_result_papi($siswa_id)
    {
        $sql = "SELECT c.aspect, b.role, a.interprestation, c.id as aspect_id FROM tbl_papi_rules a JOIN tbl_papi_roles b ON b.id=a.role_id	JOIN tbl_papi_aspects c ON c.id=b.aspect_id JOIN tbl_papi_results d ON d.role_id=b.id WHERE d.siswa_id=$siswa_id AND d.value BETWEEN a.low_value AND a.high_value ORDER BY c.id,b.id ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_result_papi_by_aspect($siswa_id, $aspect_id)
    {
        $sql = "SELECT c.aspect, b.role, a.interprestation, c.id as aspect_id FROM tbl_papi_rules a JOIN tbl_papi_roles b ON b.id=a.role_id	JOIN tbl_papi_aspects c ON c.id=b.aspect_id JOIN tbl_papi_results d ON d.role_id=b.id WHERE d.siswa_id=$siswa_id AND c.id=$aspect_id AND d.value BETWEEN a.low_value AND a.high_value ORDER BY c.id,b.id ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_role_by_id($id)
    {
        $sql = "SELECT * FROM tbl_papi_roles where aspect_id=$id ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_nilai_psikotest($siswa_id)
    {
        $sql = "SELECT tbl_jawaban_psikotest.*, SUM(tbl_jawaban_psikotest.nilai) as total_nilai, tbl_bab_psikotest.bab FROM tbl_jawaban_psikotest INNER JOIN tbl_bab_psikotest ON tbl_jawaban_psikotest.bab_id=tbl_bab_psikotest.id WHERE siswa_id=$siswa_id ORDER BY tbl_bab_psikotest.id ASC ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
