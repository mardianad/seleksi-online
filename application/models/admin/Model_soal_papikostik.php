<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_soal_papikostik extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_papi_questions'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show()
    {
        $sql = "SELECT * FROM tbl_papi_questions ORDER BY tbl_papi_questions.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function add($data)
    {
        $this->db->insert('tbl_papi_questions', $data);
        return $this->db->insert_id();
    }
    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_papi_questions', $data);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_papi_questions');
    }
    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_papi_questions WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
    function get_bab()
    {
        $sql = "SELECT * FROM tbl_papi_aspects ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function get_role($aspect_id)
    {
        $sql = "SELECT * FROM tbl_papi_roles ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function soal_papi_check($id)
    {
        $sql = 'SELECT * FROM tbl_papi_questions WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}
