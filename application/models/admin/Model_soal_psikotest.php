<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_soal_psikotest extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_soal_psikotest'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show()
    {
        $sql = "SELECT tbl_soal_psikotest.*, tbl_bab_psikotest.bab  FROM tbl_soal_psikotest JOIN tbl_bab_psikotest ON tbl_bab_psikotest.id = tbl_soal_psikotest.bab_id ORDER BY tbl_soal_psikotest.id DESC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function add($data)
    {
        $this->db->insert('tbl_soal_psikotest', $data);
        return $this->db->insert_id();
    }
    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_soal_psikotest', $data);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_soal_psikotest');
    }
    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_soal_psikotest WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
    function get_kj($soal_id)
    {
        $sql = "SELECT * FROM tbl_soal_psikotest_kj WHERE soal_id=$soal_id ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function get_bab()
    {
        $sql = "SELECT * FROM tbl_bab_psikotest ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function soal_psikotest_check($id)
    {
        $sql = 'SELECT * FROM tbl_soal_psikotest WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
    function add_kj($data)
    {
        $this->db->insert('tbl_soal_psikotest_kj', $data);
        return $this->db->insert_id();
    }
    function update_kj($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_soal_psikotest_kj', $data);
    }
    function delete_kj($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_soal_psikotest_kj');
    }
    function getData_kj($id)
    {
        $sql = 'SELECT * FROM tbl_soal_psikotest_kj WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}
