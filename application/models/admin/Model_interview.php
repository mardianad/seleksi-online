<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_interview extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_nilai_interview'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function showByID($siswa_id)
    {
        $sql = 'SELECT * FROM tbl_nilai_interview WHERE siswa_id=?';
        $query = $this->db->query($sql, array($siswa_id));
        return $query->first_row('array');
    }
    function add($data)
    {
        $this->db->insert('tbl_nilai_interview', $data);
        return $this->db->insert_id();
    }
    function update($id, $data)
    {
        $this->db->where('siswa_id', $id);
        $this->db->update('tbl_nilai_interview', $data);
    }
    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_nilai_interview WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
    function nilai_check($id)
    {
        $sql = 'SELECT * FROM tbl_nilai_interview WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}
