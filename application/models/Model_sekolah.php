<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_sekolah extends CI_Model
{
    function show()
    {
        $sql = "SELECT * FROM tbl_sekolah ORDER BY tbl_sekolah.title ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function showJurusan()
    {
        $sql = "SELECT * FROM tbl_jurusan ORDER BY title ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
