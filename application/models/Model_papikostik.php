<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Model_papikostik extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_papi_results'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_bab_limit()
    {
        $sql = "SELECT * FROM tbl_papi_aspects ORDER BY id ASC LIMIT 1";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function check_bab($id)
    {
        $sql = "SELECT * FROM tbl_papi_aspects WHERE id=$id";
        $query = $this->db->query($sql);
        return $query->first_row('array');
    }
    function show_bab()
    {
        $sql = "SELECT * FROM tbl_papi_aspects ORDER BY id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_soal()
    {
        $sql = "SELECT * FROM tbl_papi_questions ORDER BY tbl_papi_questions.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function show_soal_array()
    {
        $sql = "SELECT * FROM tbl_papi_questions ORDER BY tbl_papi_questions.id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    function add($data)
    {
        $this->db->insert('tbl_papi_results', $data);
        return $this->db->insert_id();
    }
    function add_result($data)
    {
        $sql = "INSERT INTO tbl_papi_results(siswa_id,role_id,value,created) VALUES" . implode(',', $data) . "";
        return $this->db->query($sql);
    }
    function view()
    {
        $sql = "SELECT c.aspect, b.role, a.interprestation FROM tbl_papi_rules a JOIN tbl_papi_roles b ON b.id=a.role_id	JOIN tbl_papi_aspects c ON c.id=b.aspect_id JOIN tbl_papi_results d ON d.role_id=b.id WHERE d.value BETWEEN a.low_value AND a.high_value ORDER BY c.id,b.id;";
        return $this->db->query($sql);
    }
    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_papi_results', $data);
    }
    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_papi_results');
    }
    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_papi_results WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}
