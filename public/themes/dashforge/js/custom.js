function imgError(image) {
	image.onerror = "";
	image.src = "public/users/thumbnoimage.jpg";
	return true;
}

// Preloader
$(window).on("load", function () {
	//Old Preloader Jquery
	/*$("#preloader").fadeOut();
	$("#logo_preloader").delay(250).fadeOut("slow");
	$("body").delay(250).css({
		"overflow-x": "hidden",
	});*/

	// New Preloader Jquery
	$(".preloader").fadeOut();
	// Toastr alert
	$(".toast").toast("show");
});
