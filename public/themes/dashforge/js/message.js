$(document).ready(function(){
    show_message();	//call function show all message
    
    $('#mydata').dataTable();
     
    //function show all message
    function show_message(){
        $.ajax({
            type  : 'ajax',
            url   : "<?php echo site_url('eoffice/message/message_data')?>",
            async : false,
            dataType : 'json',
            success : function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<tr>'+
                              '<td>'+data[i].message_id+'</td>'+
                            '<td>'+data[i].message_text+'</td>'+                            
                            '<td style="text-align:right;">'+
                                '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-message_id="'+data[i].message_id+'" data-message_text="'+data[i].message_text+'">Edit</a>'+' '+
                                '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-message_id="'+data[i].message_id+'">Delete</a>'+
                            '</td>'+
                            '</tr>';
                }
                $('#show_data').html(html);
            }

        });
    }

    //Save message
    $('#btn_save').on('click',function(){
        var message_id = $('#message_id').val();
        var message_text = $('#message_text').val()        
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('eoffice/message/save')?>",
            dataType : "JSON",
            data : {message_id:message_id , message_text:message_text},
            success: function(data){
                $('[name="message_id"]').val("");
                $('[name="message_text"]').val("");                
                $('#Modal_Add').modal('hide');
                show_message();
            }
        });
        return false;
    });

    //get data for update record
    $('#show_data').on('click','.item_edit',function(){
        var message_id = $(this).data('message_id');
        var message_text = $(this).data('message_text');        
        
        $('#Modal_Edit').modal('show');
        $('[name="message_id_edit"]').val(message_id);
        $('[name="message_text_edit"]').val(message_text);        
    });

    //update record to database
     $('#btn_update').on('click',function(){
        var message_id = $('#message_id_edit').val();
        var message_text = $('#message_text_edit').val()        
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('eoffice/message/update')?>",
            dataType : "JSON",
            data : {message_id:message_id , message_text:message_text},
            success: function(data){
                $('[name="message_id_edit"]').val("");
                $('[name="message_text_edit"]').val("");                
                $('#Modal_Edit').modal('hide');
                show_message();
            }
        });
        return false;
    });

    //get data for delete record
    $('#show_data').on('click','.item_delete',function(){
        var message_id = $(this).data('message_id');
        
        $('#Modal_Delete').modal('show');
        $('[name="message_id_delete"]').val(message_id);
    });

    //delete record to database
     $('#btn_delete').on('click',function(){
        var message_id = $('#message_id_delete').val();
        $.ajax({
            type : "POST",
            url  : "<?php echo site_url('eoffice/message/delete')?>",
            dataType : "JSON",
            data : {message_id:message_id},
            success: function(data){
                $('[name="message_id_delete"]').val("");
                $('#Modal_Delete').modal('hide');
                show_message();
            }
        });
        return false;
    });

});